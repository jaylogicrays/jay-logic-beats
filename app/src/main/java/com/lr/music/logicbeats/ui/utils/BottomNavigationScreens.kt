package com.lr.music.logicbeats.ui.utils

import androidx.annotation.StringRes
import com.lr.music.logicbeats.R

sealed class BottomNavigationScreens(val route: String, val icon: Int) {
    object Home : BottomNavigationScreens("Home", R.drawable.ic_home)
    object MusicLibrary : BottomNavigationScreens("Music Library", R.drawable.ic_music_lib)
    object Favourite : BottomNavigationScreens("Favourite",R.drawable.ic_fav)
    object Setting : BottomNavigationScreens("Setting", R.drawable.ic_setting)
}

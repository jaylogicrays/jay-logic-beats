package com.lr.music.logicbeats.ui.screen.home_screen

import android.content.Context
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Arrangement.Center
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.IconButton
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.paint
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import coil.compose.rememberAsyncImagePainter
import com.lr.music.logicbeats.R
import com.lr.music.logicbeats.common.CustomSearchView
import com.lr.music.logicbeats.common.CustomTextView
import com.lr.music.logicbeats.model.home_screen.DiscoverMusicModel
import com.lr.music.logicbeats.model.home_screen.DiscoverTitleDataModel
import com.lr.music.logicbeats.navigation.RoutePath

@Composable
fun HomeScreen(navController: NavController, homeScreenViewModel: HomeScreenViewModel = viewModel()) {
    val context = LocalContext.current
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Black)
            .padding(top = 27.dp, start = 14.dp, end = 14.dp)
            .verticalScroll(rememberScrollState())
    ) {
        UserViewUI(homeScreenViewModel, navController,context)
        SearchMusicUI(homeScreenViewModel, context)
        MusicOptionUI(homeScreenViewModel, navController, context)
        TrendingMusicListUI(homeScreenViewModel, navController, context)
        TrendingAlbumUI(homeScreenViewModel, context)
        UserPlayList(homeScreenViewModel, context)
        NewMorningSongUI(homeScreenViewModel, context)
        MadeForYouSongUI(homeScreenViewModel, context)
        DiscoverUI(homeScreenViewModel, context)
    }
}

@Composable
fun UserViewUI(homeScreenViewModel: HomeScreenViewModel, navController: NavController, context: Context) {
    Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            CustomTextView(
                textData = "Hello ${homeScreenViewModel.userName}",
                fontSize = 22,
                fontFamily = FontFamily(Font(R.font.lexend_semi_bold)),
                modifier = Modifier,
                textAlignment = TextAlign.Start
            )
            Image(
                modifier = Modifier
                    .padding(start = 10.dp)
                    .size(20.dp),
                painter = painterResource(id = R.drawable.ic_star),
                contentDescription = "star_image"
            )
        }
        Row(verticalAlignment = Alignment.CenterVertically) {
            IconButton(
                onClick = {
                    navController.navigate(RoutePath.NotificationScreen.name)
                },
                modifier = Modifier
                    .background(color = colorResource(id = R.color.container_color), shape = RoundedCornerShape(6.dp))
                    .size(30.dp),
            ) {
                Image(
                    painter = painterResource(id = R.drawable.ic_notification),
                    contentDescription = "Image",
                    modifier = Modifier.size(18.dp)
                )
            }
            Image(
                painter = rememberAsyncImagePainter(homeScreenViewModel.profileImage),
                contentDescription = "Image",
                modifier = Modifier
                    .padding(start = 14.dp)
                    .size(30.dp)
                    .clip(CircleShape).clickable {
                        navController.navigate(RoutePath.EditProfileScreen.name)
                    },
                contentScale = ContentScale.Crop
            )

        }
    }
    CustomTextView(
        textData = context.getString(R.string.welcome_account_text),
        fontSize = 14,
        textColor = colorResource(id = R.color.silver_chalice_color),
        fontFamily = FontFamily(Font(R.font.lexend_regular)),
        modifier = Modifier.padding(top = 9.dp),
        textAlignment = TextAlign.Start
    )
}

@Composable
fun SearchMusicUI(homeScreenViewModel: HomeScreenViewModel, context: Context) {
    CustomSearchView(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 29.dp)
            .shadow(elevation = 5.dp, shape = RoundedCornerShape(10.dp))
            .clip(RoundedCornerShape(10.dp))
            .height(50.dp),
        hintsString = context.getString(R.string.search_hint_text),
        textField = homeScreenViewModel.textField.value
    ) {
        homeScreenViewModel.textField.value = it
    }
}

@Composable
fun MusicOptionUI(homeScreenViewModel: HomeScreenViewModel, navController: NavController, context: Context, ) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 25.dp)
    ) {
        IconButton(
            onClick = {
                navController.navigate(RoutePath.LikedSongScreen.name)
            },
            modifier = Modifier
                .background(color = colorResource(id = R.color.purple_color), shape = RoundedCornerShape(6.dp))
                .height(64.dp)
                .weight(1f),
        ) {
            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                Image(
                    painter = painterResource(id = R.drawable.ic_favourite),
                    contentDescription = "Image",                    modifier = Modifier.size(16.dp)
                )
                CustomTextView(
                    textData = context.getString(R.string.favourites_text),
                    fontSize = 14,
                    textColor = colorResource(id = R.color.white),
                    fontFamily = FontFamily(Font(R.font.lexend_regular)),
                    modifier = Modifier.padding(top = 5.dp),
                    textAlignment = TextAlign.Start
                )
            }
        }
        Box(modifier = Modifier.weight(.1f))
        IconButton(
            onClick = {
                navController.navigate(RoutePath.PlayListScreen.name)
            },
            modifier = Modifier
                .background(color = colorResource(id = R.color.pink_color), shape = RoundedCornerShape(6.dp))
                .height(64.dp)
                .weight(1f),
        ) {
            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                Image(
                    painter = painterResource(id = R.drawable.ic_music_library),
                    contentDescription = "Image",
                    modifier = Modifier.size(20.dp)
                )
                CustomTextView(
                    textData = context.getString(R.string.playlist_text),
                    fontSize = 14,
                    textColor = colorResource(id = R.color.white),
                    fontFamily = FontFamily(Font(R.font.lexend_regular)),
                    modifier = Modifier.padding(top = 5.dp),
                    textAlignment = TextAlign.Start
                )
            }
        }
        Box(modifier = Modifier.weight(.1f))
        IconButton(
            onClick = {
            },
            modifier = Modifier
                .weight(1f)
                .background(color = colorResource(id = R.color.orange_color), shape = RoundedCornerShape(6.dp))
                .height(64.dp),
        ) {
            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                Image(
                    painter = painterResource(id = R.drawable.ic_recent),
                    contentDescription = "Image",
                    modifier = Modifier.size(16.dp)
                )
                CustomTextView(
                    textData = context.getString(R.string.recent_text),
                    fontSize = 14,
                    textColor = colorResource(id = R.color.white),
                    fontFamily = FontFamily(Font(R.font.lexend_regular)),
                    modifier = Modifier.padding(top = 5.dp),
                    textAlignment = TextAlign.Start
                )
            }
        }
    }
}

@Composable
fun TrendingMusicListUI(homeScreenViewModel: HomeScreenViewModel, navController: NavController, context: Context) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 30.dp), horizontalArrangement = Arrangement.SpaceBetween, verticalAlignment = Alignment.CenterVertically
    ) {
        CustomTextView(
            textData = context.getString(R.string.trending_artist_text),
            fontSize = 18,
            textColor = colorResource(id = R.color.white),
            fontFamily = FontFamily(Font(R.font.lexend_medium)),
            modifier = Modifier,
            textAlignment = TextAlign.Start
        )
        Row(verticalAlignment = Alignment.CenterVertically) {
            CustomTextView(
                textData = context.getString(R.string.see_all_text),
                fontSize = 10,
                textColor = colorResource(id = R.color.gray_color),
                fontFamily = FontFamily(Font(R.font.lexend_medium)),
                modifier = Modifier,
                textAlignment = TextAlign.Start
            )
            Image(
                painter = painterResource(id = R.drawable.ic_arrow),
                contentDescription = "Image",
                modifier = Modifier
                    .padding(start = 5.dp)
                    .size(16.dp)
            )
        }
    }

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 20.dp)
            .horizontalScroll(rememberScrollState())
    ) {
        homeScreenViewModel.trendingArtistArray.forEach { path ->
            Column(
                modifier = Modifier
                    .padding(end = 20.dp)
                    .width(60.dp).clickable {
                        navController.navigate(RoutePath.ArtistSongScreen.name)
                    }
            ) {
                Image(
                    painter = rememberAsyncImagePainter(path.profileImage),
                    contentDescription = "Artist Image",
                    modifier = Modifier
                        .size(60.dp)
                        .clip(shape = RoundedCornerShape(13.dp)), contentScale = ContentScale.FillBounds
                )
                CustomTextView(
                    textData = path.name,
                    fontSize = 10,
                    maxLines = 1,
                    textColor = colorResource(id = R.color.white),
                    fontFamily = FontFamily(Font(R.font.lexend_regular)),
                    modifier = Modifier.padding(top = 15.dp),
                    textAlignment = TextAlign.Center
                )
            }
        }
    }
}

@Composable
fun TrendingAlbumUI(homeScreenViewModel: HomeScreenViewModel, context: Context) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 30.dp), horizontalArrangement = Arrangement.SpaceBetween, verticalAlignment = Alignment.CenterVertically
    ) {
        CustomTextView(
            textData = context.getString(R.string.trending_album_text),
            fontSize = 18,
            textColor = colorResource(id = R.color.white),
            fontFamily = FontFamily(Font(R.font.lexend_medium)),
            modifier = Modifier,
            textAlignment = TextAlign.Start
        )
        Row(verticalAlignment = Alignment.CenterVertically) {
            CustomTextView(
                textData = context.getString(R.string.see_all_text),
                fontSize = 10,
                textColor = colorResource(id = R.color.gray_color),
                fontFamily = FontFamily(Font(R.font.lexend_medium)),
                modifier = Modifier,
                textAlignment = TextAlign.Start
            )
            Image(
                painter = painterResource(id = R.drawable.ic_arrow),
                contentDescription = "Image",
                modifier = Modifier
                    .padding(start = 5.dp)
                    .size(16.dp)
            )
        }
    }

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 20.dp)
            .horizontalScroll(rememberScrollState())
    ) {
        homeScreenViewModel.trendingAlbumArray.forEach { path ->
            Box(
                modifier = Modifier
                    .padding(end = 10.dp)
                    .background(shape = RoundedCornerShape(15.dp), color = colorResource(id = R.color.container_color))
                    .padding(14.dp)
                    .clickable {
                    },
                contentAlignment = Alignment.Center
            ) {
                Column(horizontalAlignment = Alignment.CenterHorizontally) {
                    CustomTextView(
                        textData = path.songName,
                        fontSize = 10,
                        textColor = colorResource(id = R.color.white),
                        fontFamily = FontFamily(Font(R.font.lexend_medium)),
                        modifier = Modifier,
                        textAlignment = TextAlign.Start
                    )
                    CustomTextView(
                        textData = path.singerName,
                        fontSize = 8,
                        textColor = colorResource(id = R.color.gray_color),
                        fontFamily = FontFamily(Font(R.font.lexend_regular)),
                        modifier = Modifier.padding(top = 5.dp),
                        textAlignment = TextAlign.Start
                    )
                    Box(
                        modifier = Modifier
                            .padding(top = 19.dp)
                            .size(76.dp)
                    ) {
                        Image(
                            painter = rememberAsyncImagePainter(path.musicImage),
                            contentDescription = "Artist Image",
                            modifier = Modifier
                                .fillMaxSize()
                                .clip(shape = CircleShape), contentScale = ContentScale.FillBounds
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun UserPlayList(homeScreenViewModel: HomeScreenViewModel, context: Context){
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 30.dp), horizontalArrangement = Arrangement.SpaceBetween, verticalAlignment = Alignment.CenterVertically
    ) {
        CustomTextView(
            textData = context.getString(R.string.user_play_list_text),
            fontSize = 18,
            textColor = colorResource(id = R.color.white),
            fontFamily = FontFamily(Font(R.font.lexend_medium)),
            modifier = Modifier,
            textAlignment = TextAlign.Start
        )
        Row(verticalAlignment = Alignment.CenterVertically) {
            CustomTextView(
                textData = context.getString(R.string.see_all_text),
                fontSize = 10,
                textColor = colorResource(id = R.color.gray_color),
                fontFamily = FontFamily(Font(R.font.lexend_medium)),
                modifier = Modifier,
                textAlignment = TextAlign.Start
            )
            Image(
                painter = painterResource(id = R.drawable.ic_arrow),
                contentDescription = "Image",
                modifier = Modifier
                    .padding(start = 5.dp)
                    .size(16.dp)
            )
        }
    }

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 20.dp)
            .horizontalScroll(rememberScrollState())
    ) {
        homeScreenViewModel.userPlaylistArray.forEach { path ->
            Box(
                modifier = Modifier
                    .padding(end = 10.dp)
                    .clip(shape = RoundedCornerShape(16.dp))
                    .width(162.dp)
                    .height(180.dp)
                    .paint(
                        rememberAsyncImagePainter(path.albumImage),
                        contentScale = ContentScale.FillBounds
                    )
                    .clickable {
                    },
                contentAlignment = Alignment.BottomEnd
            ) {
                Surface(
                    color = colorResource(id = R.color.onyx_color).copy(alpha = 0.6f),
                    modifier = Modifier
                        .width(153.dp)
                        .height(50.dp)
                ) {
                    Column(
                        modifier = Modifier
                            .fillMaxHeight()
                            .padding(start = 14.dp), verticalArrangement = Center
                    ) {
                        CustomTextView(
                            textData = path.albumTitle,
                            fontSize = 14,
                            textColor = colorResource(id = R.color.white),
                            fontFamily = FontFamily(Font(R.font.lexend_medium)),
                            modifier = Modifier,
                            textAlignment = TextAlign.Start
                        )
                        CustomTextView(
                            textData = path.albumDescription,
                            fontSize = 12,
                            textColor = colorResource(id = R.color.gray_color),
                            fontFamily = FontFamily(Font(R.font.lexend_regular)),
                            modifier = Modifier,
                            textAlignment = TextAlign.Start
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun NewMorningSongUI(homeScreenViewModel: HomeScreenViewModel, context: Context) {
    Box(modifier = Modifier.padding(top = 30.dp), contentAlignment = Alignment.BottomEnd) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .height(151.dp)
                .paint(
                    painterResource(id = R.drawable.ic_rectangle_border),
                    contentScale = ContentScale.FillBounds
                ),
        ) {
            Column(modifier = Modifier.padding(15.dp)) {
                CustomTextView(
                    textData = context.getString(R.string.new_morning_song_text),
                    fontSize = 24,
                    lineHeight = 30,
                    textColor = colorResource(id = R.color.white),
                    fontFamily = FontFamily(Font(R.font.lexend_bold)),
                    textAlignment = TextAlign.Start
                )
                CustomTextView(
                    textData = "Discover ${homeScreenViewModel.discoverSongCount} songs",
                    fontSize = 14,
                    textColor = colorResource(id = R.color.gray_color),
                    fontFamily = FontFamily(Font(R.font.lexend_regular)),
                    modifier = Modifier.padding(top = 10.dp),
                    textAlignment = TextAlign.Start
                )
                Image(
                    painter = painterResource(id = R.drawable.ic_arrow_right),
                    contentDescription = "Next Image",
                    modifier = Modifier
                        .padding(top = 16.dp)
                        .size(18.dp), contentScale = ContentScale.FillBounds
                )
            }
        }
        Image(
            painter = painterResource(id = R.drawable.ic_person),
            contentDescription = "Person Image",
            modifier = Modifier
                .width(138.dp)
                .height(181.dp)
                .offset(y = 10.dp), contentScale = ContentScale.FillBounds
        )
    }
}

@Composable
fun MadeForYouSongUI(homeScreenViewModel: HomeScreenViewModel, context: Context) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 30.dp), horizontalArrangement = Arrangement.SpaceBetween, verticalAlignment = Alignment.CenterVertically
    ) {
        CustomTextView(
            textData = context.getString(R.string.made_for_you_text),
            fontSize = 18,
            textColor = colorResource(id = R.color.white),
            fontFamily = FontFamily(Font(R.font.lexend_medium)),
            modifier = Modifier,
            textAlignment = TextAlign.Start
        )
        Row(verticalAlignment = Alignment.CenterVertically) {
            CustomTextView(
                textData = context.getString(R.string.see_all_text),
                fontSize = 10,
                textColor = colorResource(id = R.color.gray_color),
                fontFamily = FontFamily(Font(R.font.lexend_medium)),
                modifier = Modifier,
                textAlignment = TextAlign.Start
            )
            Image(
                painter = painterResource(id = R.drawable.ic_arrow),
                contentDescription = "Image",
                modifier = Modifier
                    .padding(start = 5.dp)
                    .size(16.dp)
            )
        }
    }

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 20.dp)
            .horizontalScroll(rememberScrollState())
    ) {
        homeScreenViewModel.madeForYouArray.forEach { path ->
            Box(
                modifier = Modifier
                    .padding(end = 10.dp)
                    .clip(shape = RoundedCornerShape(16.dp))
                    .size(153.dp)
                    .paint(
                        rememberAsyncImagePainter(path.albumImage),
                        contentScale = ContentScale.FillBounds
                    )
                    .clickable {
                    },
                contentAlignment = Alignment.BottomEnd
            ) {
                Surface(
                    color = colorResource(id = R.color.onyx_color).copy(alpha = 0.6f),
                    modifier = Modifier
                        .width(153.dp)
                        .height(50.dp)
                ) {
                    Column(
                        modifier = Modifier
                            .fillMaxHeight()
                            .padding(start = 14.dp), verticalArrangement = Center
                    ) {
                        CustomTextView(
                            textData = path.albumTitle,
                            fontSize = 14,
                            textColor = colorResource(id = R.color.white),
                            fontFamily = FontFamily(Font(R.font.lexend_medium)),
                            modifier = Modifier,
                            textAlignment = TextAlign.Start
                        )
                        CustomTextView(
                            textData = path.albumDescription,
                            fontSize = 12,
                            textColor = colorResource(id = R.color.gray_color),
                            fontFamily = FontFamily(Font(R.font.lexend_regular)),
                            modifier = Modifier,
                            textAlignment = TextAlign.Start
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun DiscoverUI(homeScreenViewModel: HomeScreenViewModel, context: Context) {
    CustomTextView(
        textData = context.getString(R.string.discover_text),
        fontSize = 18,
        textColor = colorResource(id = R.color.white),
        fontFamily = FontFamily(Font(R.font.lexend_medium)),
        modifier = Modifier.padding(top = 30.dp),
        textAlignment = TextAlign.Start
    )

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 14.dp)
    ) {
        homeScreenViewModel.discoverTitleArray.forEachIndexed { index, musicItem ->
            DiscoverSongItem(musicItem, index == (homeScreenViewModel.discoverTitleArray.size - 1), onClick = {
                homeScreenViewModel.discoverTitleArray[homeScreenViewModel.discoverSongSelectedIndex] =
                    homeScreenViewModel.discoverTitleArray[homeScreenViewModel.discoverSongSelectedIndex].copy(isSelected = false)
                homeScreenViewModel.discoverSongSelectedIndex = index
                homeScreenViewModel.discoverTitleArray[index] =
                    homeScreenViewModel.discoverTitleArray[index].copy(isSelected = true)
            })
        }
    }

    Column(
        modifier = Modifier
            .heightIn(8.dp, 300.dp)
            .verticalScroll(rememberScrollState())
    ) {
        homeScreenViewModel.discoverTitleArray[homeScreenViewModel.discoverSongSelectedIndex].discoverTitleDataArray.forEachIndexed { index, discoverTitleDataItem ->
            SelectedSongItem(discoverTitleDataItem)
        }
    }
}

@Composable
fun DiscoverSongItem(musicItem: DiscoverMusicModel, isLast: Boolean, onClick: () -> Unit) {
    Row(
        modifier = Modifier
            .defaultMinSize()
            .padding(vertical = 10.dp)
    ) {
        Box(
            modifier = Modifier
                .width((LocalConfiguration.current.screenWidthDp.dp - 20.dp) / 4)
                .height(34.dp)
                .padding(end = if (isLast) 0.dp else 10.dp)
                .clip(RoundedCornerShape(4.dp))
                .background(
                    color = if (musicItem.isSelected) colorResource(
                        id = R.color.white
                    ) else colorResource(id = R.color.container_color)
                )
                .clickable { onClick() },
            contentAlignment = Alignment.Center,
        ) {
            CustomTextView(
                textData = musicItem.discoverTitle,
                fontSize = 12,
                textColor = if (musicItem.isSelected) colorResource(
                    id = R.color.black
                ) else colorResource(id = R.color.white),
                fontFamily = FontFamily(Font(R.font.lexend_medium)),
                modifier = Modifier,
                textAlignment = TextAlign.Start
            )
        }
    }
}

@Composable
fun SelectedSongItem(discoverTitleDataItem: DiscoverTitleDataModel) {
    Column {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 18.dp), horizontalArrangement = Arrangement.SpaceBetween, verticalAlignment = Alignment.CenterVertically
        ) {
            Row(
                modifier = Modifier, verticalAlignment = Alignment.CenterVertically
            ) {
                Image(
                    painter = rememberAsyncImagePainter(discoverTitleDataItem.songImage),
                    contentDescription = "Person Image",
                    modifier = Modifier
                        .size(50.dp)
                        .clip(RoundedCornerShape(5.dp)), contentScale = ContentScale.Fit
                )
                Column(modifier = Modifier.padding(start = 17.dp)) {
                    CustomTextView(
                        textData = discoverTitleDataItem.songName,
                        fontSize = 14,
                        textColor = colorResource(id = R.color.white),
                        fontFamily = FontFamily(Font(R.font.lexend_medium)),
                        modifier = Modifier,
                        textAlignment = TextAlign.Start
                    )
                    Row(modifier = Modifier.padding(top = 7.dp), verticalAlignment = Alignment.CenterVertically) {
                        CustomTextView(
                            textData = discoverTitleDataItem.songDuration,
                            fontSize = 10,
                            textColor = colorResource(id = R.color.gray_color),
                            fontFamily = FontFamily(Font(R.font.lexend_regular)),
                            modifier = Modifier,
                            textAlignment = TextAlign.Start,
                        )
                        Box(
                            modifier = Modifier
                                .padding(horizontal = 5.dp)
                                .size(3.dp)
                                .clip(CircleShape)
                                .background(colorResource(id = R.color.gray_color))
                        )
                        CustomTextView(
                            textData = discoverTitleDataItem.singerName,
                            fontSize = 10,
                            textColor = colorResource(id = R.color.gray_color),
                            fontFamily = FontFamily(Font(R.font.lexend_regular)),
                            modifier = Modifier,
                            textAlignment = TextAlign.Start,
                        )
                    }
                }
            }
            Row(verticalAlignment = Alignment.CenterVertically) {
                Box(modifier = Modifier.size(32.dp), contentAlignment = Alignment.Center){
                    Image(
                        painter = painterResource(id = if (discoverTitleDataItem.isLiked) R.drawable.ic_like else R.drawable.ic_unlike),
                        contentDescription = "Like Image",
                        modifier = Modifier
                            .size(22.dp),
                        contentScale = ContentScale.Fit
                    )
                }

                Box(
                    modifier = Modifier
                        .padding(start = 14.dp)
                        .size(30.dp)
                        .clip(CircleShape)
                        .background(
                            color = if (discoverTitleDataItem.isPlaying) colorResource(id = R.color.white) else colorResource(id = R.color.black)
                        ),
                    contentAlignment = Alignment.Center
                ) {
                    Image(
                        painter = painterResource(id = if (discoverTitleDataItem.isPlaying) R.drawable.ic_pause else R.drawable.ic_play),
                        contentDescription = "Play Pause Image",
                        modifier = Modifier
                            .size(12.dp),
                        contentScale = ContentScale.Fit
                    )
                }
            }
        }
        Spacer(
            modifier = Modifier
                .padding(top = 18.dp)
                .height(1.dp)
                .fillMaxWidth()
                .background(colorResource(id = R.color.divider_color))
        )
    }
}

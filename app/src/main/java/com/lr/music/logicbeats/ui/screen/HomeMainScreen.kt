package com.lr.music.logicbeats.ui.screen

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Icon
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.NavigationBarItemDefaults
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.lr.music.logicbeats.R
import com.lr.music.logicbeats.ui.screen.home_screen.HomeScreen
import com.lr.music.logicbeats.ui.screen.liked_screen.LikedSongScreen
import com.lr.music.logicbeats.ui.screen.your_library.YourLibraryScreen
import com.lr.music.logicbeats.ui.utils.BottomNavigationScreens

@Composable
fun HomeMainScreen(navControllerMain: NavHostController) {
    val navController = rememberNavController()

    val bottomNavigationItems = listOf(
        BottomNavigationScreens.Home,
        BottomNavigationScreens.MusicLibrary,
        BottomNavigationScreens.Favourite,
        BottomNavigationScreens.Setting
    )
    Scaffold(
        bottomBar = {
            SpookyAppBottomNavigation(navController, bottomNavigationItems)
        },
    ){ innerPadding ->
        Box(modifier = Modifier.fillMaxSize()
            .background(colorResource(id = R.color.black))
            .padding(innerPadding)) {
            MainScreenNavigationConfigurations(navController, navControllerMain)
        }
    }
}

@Composable
private fun SpookyAppBottomNavigation(
    navController: NavHostController,
    items: List<BottomNavigationScreens>
) {
    NavigationBar(containerColor = colorResource(id = R.color.container_color)) {
        val currentRoute = currentRoute(navController)
        items.forEach { screen ->
            NavigationBarItem(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(80.dp)
                    .background(colorResource(id = R.color.container_color)),
                icon = {
                    Icon(
                        painter = painterResource(id = screen.icon),
                        contentDescription = "Icon",
                        modifier = Modifier.size(30.dp),
                        tint = if (currentRoute == screen.route) colorResource(
                            id = R.color.white
                        ) else colorResource(id = R.color.silver_chalice_color)
                    )
                },
               /* label = {
                    Text(
//                        stringResource(id = screen.resourceId),
                        style = TextStyle(
                            fontSize = 8.sp,
                            fontFamily = FontFamily(Font(R.font.nunitosans_10pt_regular)),
                            color = if (currentRoute == screen.route) colorResource(
                                id = R.color.selected_item_color
                            ) else Color.White
                        )
                    )
                },*/
                selected = currentRoute == screen.route,
                alwaysShowLabel = true, // This hides the title for the unselected items
                onClick = {
                    if (currentRoute != screen.route) {
                        navController.navigate(screen.route)
                    }
                },
                colors = NavigationBarItemDefaults
                    .colors(
                        indicatorColor = colorResource(id = R.color.container_color)
                    )
            )
        }
    }
}

@Composable
private fun currentRoute(navController: NavHostController): String? {
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    return navBackStackEntry?.destination?.route
}

@Composable
private fun MainScreenNavigationConfigurations(
    navController: NavHostController, navControllerMain: NavController
) {
    NavHost(navController, startDestination =BottomNavigationScreens.Home.route) {
        composable(BottomNavigationScreens.Home.route) {
            HomeScreen(navControllerMain)
        }
        composable(BottomNavigationScreens.MusicLibrary.route) {
            YourLibraryScreen(navController = navControllerMain)
        }
        composable(BottomNavigationScreens.Favourite.route) {
            LikedSongScreen(navController = navControllerMain)
        }
        composable(BottomNavigationScreens.Setting.route) {
//            MyWalletScreen(navController =navControllerMain)
        }
    }
}
package com.lr.music.logicbeats.ui.screen.artist_song

import android.content.Context
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import coil.compose.rememberAsyncImagePainter
import com.lr.music.logicbeats.R
import com.lr.music.logicbeats.common.CustomTextView
import com.lr.music.logicbeats.common.TopTitleWithCenterText
import com.lr.music.logicbeats.model.artist_song.ArtistSongModel

@Composable
fun ArtistSongScreen(navController: NavHostController, artistSongScreenViewModel: ArtistSongScreenViewModel = viewModel()) {
    /*WindowCompat.setDecorFitsSystemWindows(window, false)
    window.setFlags(
        WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
        WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
    )*/
    val context = LocalContext.current
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(colorResource(id = R.color.black))
    ) {
        ImageUI(artistSongScreenViewModel, navController, context)
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 14.dp)
                .verticalScroll(rememberScrollState())
        ) {
            PlaylistUI(artistSongScreenViewModel, context)
            artistSongScreenViewModel.artistSongArray.forEach { artistModel ->
                SelectedSongItem(artistModel)
            }
        }
    }
}

@Composable
fun TitleBarUI(navController: NavController, context: Context) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 28.dp, start = 14.dp)
    ) {
        TopTitleWithCenterText(centerTitle = "") {
            navController.navigateUp()
        }
    }
}

@Composable
fun ImageUI(artistSongScreenViewModel: ArtistSongScreenViewModel, navController: NavHostController, context: Context) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height(279.dp)
    ) {
        Image(
            painter = rememberAsyncImagePainter(model = artistSongScreenViewModel.singerImage),
            contentDescription = "singer_image",
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 25.dp)
                .clip(RoundedCornerShape(bottomStart = 30.dp, bottomEnd = 30.dp)), contentScale = ContentScale.Crop
        )
        Column(modifier = Modifier.fillMaxSize(), verticalArrangement = Arrangement.SpaceBetween) {
            TitleBarUI(navController = navController, context = context)
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 14.dp), verticalArrangement = Arrangement.SpaceBetween
            ) {
                CustomTextView(
                    textData = artistSongScreenViewModel.artistAlbumName,
                    fontSize = 12,
                    textColor = colorResource(id = R.color.silver_chalice_color),
                    fontFamily = FontFamily(Font(R.font.lexend_semi_bold)),
                    modifier = Modifier,
                    textAlignment = TextAlign.Start
                )
                CustomTextView(
                    textData = artistSongScreenViewModel.artistTitleName,
                    fontSize = 18,
                    textColor = colorResource(id = R.color.white),
                    fontFamily = FontFamily(Font(R.font.lexend_semi_bold)),
                    modifier = Modifier.padding(top = 10.dp),
                    textAlignment = TextAlign.Start
                )
                Box(modifier = Modifier.fillMaxWidth(), contentAlignment = Alignment.BottomEnd) {
                    Image(
                        painter = painterResource(id = R.drawable.ic_play_button),
                        contentDescription = "play_icon",
                        modifier = Modifier
                            .padding(end = 23.dp)
                            .size(50.dp),
                    )
                }
            }
        }
    }
}

@Composable
fun PlaylistUI(artistSongScreenViewModel: ArtistSongScreenViewModel, context: Context) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 10.dp), horizontalArrangement = Arrangement.SpaceBetween, verticalAlignment = Alignment.CenterVertically
    ) {
        CustomTextView(
            textData = context.getString(R.string.playlist_text),
            fontSize = 16,
            textColor = colorResource(id = R.color.white),
            fontFamily = FontFamily(Font(R.font.lexend_bold)),
            modifier = Modifier,
            textAlignment = TextAlign.Start
        )
        Row {
            IconButton(onClick = { artistSongScreenViewModel.isShuffleSelected = !artistSongScreenViewModel.isShuffleSelected }) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_suffle),
                    contentDescription = "shuffle_image",
                    modifier = Modifier.size(16.dp),
                    tint = colorResource(
                        id = if (artistSongScreenViewModel.isShuffleSelected) R.color.light_blue_color else R.color.white
                    )
                )
            }
            IconButton(onClick = { artistSongScreenViewModel.isRepeatSelected = !artistSongScreenViewModel.isRepeatSelected }) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_repeat),
                    contentDescription = "repeat_image",
                    modifier = Modifier.size(16.dp),
                    tint = colorResource(
                        id = if (artistSongScreenViewModel.isRepeatSelected) R.color.light_blue_color else R.color.white
                    )
                )
            }
        }
    }
}


@Composable
fun SelectedSongItem(selectedSongItem: ArtistSongModel) {
    Column(modifier = Modifier) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 18.dp), horizontalArrangement = Arrangement.SpaceBetween, verticalAlignment = Alignment.CenterVertically
        ) {
            Row(
                modifier = Modifier, verticalAlignment = Alignment.CenterVertically
            ) {
                Image(
                    painter = rememberAsyncImagePainter(selectedSongItem.songImage),
                    contentDescription = "Person Image",
                    modifier = Modifier
                        .size(50.dp)
                        .clip(RoundedCornerShape(10.dp)), contentScale = ContentScale.Crop
                )
                Column(modifier = Modifier.padding(start = 17.dp)) {
                    CustomTextView(
                        textData = selectedSongItem.songName,
                        fontSize = 14,
                        textColor = colorResource(id = R.color.white),
                        fontFamily = FontFamily(Font(R.font.lexend_medium)),
                        modifier = Modifier,
                        textAlignment = TextAlign.Start
                    )
                    Row(modifier = Modifier.padding(top = 7.dp), verticalAlignment = Alignment.CenterVertically) {
                        CustomTextView(
                            textData = selectedSongItem.songDuration,
                            fontSize = 10,
                            textColor = colorResource(id = R.color.gray_color),
                            fontFamily = FontFamily(Font(R.font.lexend_regular)),
                            modifier = Modifier,
                            textAlignment = TextAlign.Start,
                        )
                        Box(
                            modifier = Modifier
                                .padding(horizontal = 5.dp)
                                .size(3.dp)
                                .clip(CircleShape)
                                .background(colorResource(id = R.color.gray_color))
                        )
                        CustomTextView(
                            textData = selectedSongItem.singerName,
                            fontSize = 10,
                            textColor = colorResource(id = R.color.gray_color),
                            fontFamily = FontFamily(Font(R.font.lexend_regular)),
                            modifier = Modifier,
                            textAlignment = TextAlign.Start,
                        )
                    }
                }
            }
        }
        Spacer(
            modifier = Modifier
                .padding(top = 18.dp)
                .height(1.dp)
                .fillMaxWidth()
                .background(colorResource(id = R.color.divider_color))
        )
    }
}

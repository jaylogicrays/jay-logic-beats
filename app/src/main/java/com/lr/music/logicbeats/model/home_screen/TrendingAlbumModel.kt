package com.lr.music.logicbeats.model.home_screen

data class TrendingAlbumModel(
    val musicImage : String,
    val songName: String,
    val singerName: String,
)

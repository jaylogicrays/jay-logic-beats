package com.lr.music.logicbeats.ui.screen.setting

import android.content.Context
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import coil.compose.rememberAsyncImagePainter
import com.lr.music.logicbeats.R
import com.lr.music.logicbeats.common.CustomTextView
import com.lr.music.logicbeats.common.TopTitleWithCenterText
import com.lr.music.logicbeats.common.VerticalSpacer

@Composable
fun SettingScreen(navController: NavHostController, settingViewModel: SettingViewModel = viewModel()) {
    var context = LocalContext.current
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(colorResource(id = R.color.black))
    ) {
        TitleBarUI(navController = navController, context = context, settingViewModel)
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 14.dp, vertical = 15.dp)
                .verticalScroll(rememberScrollState())
        ) {
            ProfileUI(settingViewModel, context)
            EditProfileUI(context)
            MusicPlaybackUI(context)
            AboutUI(context)
        }
    }
}

@Composable
fun TitleBarUI(navController: NavController, context: Context, settingViewModel: SettingViewModel) {
    Row(modifier = Modifier.fillMaxWidth()) {
        TopTitleWithCenterText(centerTitle = context.getString(R.string.setting_text)) {
            navController.navigateUp()
        }
    }
}

@Composable
fun ProfileUI(settingViewModel: SettingViewModel, context: Context) {
    Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.SpaceBetween) {
        Row {
            Image(
                painter = rememberAsyncImagePainter(model = settingViewModel.profileImage),
                contentDescription = "profileImage",
                modifier = Modifier
                    .size(40.dp)
                    .clip(CircleShape)
            )
            Column(modifier = Modifier.padding(start = 14.dp)) {
                CustomTextView(
                    textData = settingViewModel.userName,
                    fontSize = 16,
                    textColor = colorResource(id = R.color.white),
                    fontFamily = FontFamily(Font(R.font.lexend_medium)),
                    modifier = Modifier,
                    textAlignment = TextAlign.Start
                )
                CustomTextView(
                    textData = context.getString(R.string.view_profile_text),
                    fontSize = 14,
                    textColor = colorResource(id = R.color.gray_color),
                    fontFamily = FontFamily(Font(R.font.lexend_regular)),
                    modifier = Modifier.padding(top = 7.dp),
                    textAlignment = TextAlign.Start
                )
            }
        }
        Box(
            modifier = Modifier
                .padding(end = 12.dp)
                .size(20.dp), contentAlignment = Alignment.Center
        ) {
            Image(
                painter = painterResource(id = R.drawable.ic_next), contentDescription = "nextImage", modifier = Modifier
                    .width(8.dp)
                    .height(14.dp)
            )
        }
    }
}

@Composable
fun EditProfileUI(context: Context) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 20.dp)
            .background(color = colorResource(id = R.color.playlist_bg_color), shape = RoundedCornerShape(4.dp))
    ) {
        Column {
            ItemUI(context.getString(R.string.edit_profile_text), fontFamily = FontFamily(Font(R.font.lexend_semi_bold)),
                onClick = {})
            VerticalSpacer(1, colorResource(id = R.color.divider_color))
            ItemUI(context.getString(R.string.try_logic_beat_pro_text), fontFamily = FontFamily(Font(R.font.lexend_semi_bold)),
                onClick = {})
        }
    }
}

@Composable
fun MusicPlaybackUI(context: Context) {
    CustomTextView(
        textData = context.getString(R.string.music_and_playback_text),
        fontSize = 16,
        textColor = colorResource(id = R.color.white),
        fontFamily = FontFamily(Font(R.font.lexend_semi_bold)),
        modifier = Modifier.padding(top = 30.dp),
        textAlignment = TextAlign.Start
    )
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 20.dp)
            .background(color = colorResource(id = R.color.playlist_bg_color), shape = RoundedCornerShape(4.dp))
    ) {
        Column {
            ItemUI(context.getString(R.string.music_language_text), showSecondText = "Hindi", showText = true, fontSize = 12,
                onClick = {})
            VerticalSpacer(1, colorResource(id = R.color.divider_color))
            ItemUI(context.getString(R.string.artist_selection_text),
                onClick = {})
            VerticalSpacer(1, colorResource(id = R.color.divider_color))
            ItemUI(context.getString(R.string.eqalizer_text),
                onClick = {})
            VerticalSpacer(1, colorResource(id = R.color.divider_color))
            ItemUI(context.getString(R.string.sleep_timer_text), showSecondText = "Off", showText = true,
                onClick = {})
            VerticalSpacer(1, colorResource(id = R.color.divider_color))
            SimpleTextUI(context.getString(R.string.clear_recent_history_text),
                onClick = {})
            VerticalSpacer(1, colorResource(id = R.color.divider_color))
            SimpleTextUI(context.getString(R.string.clear_recent_search_text),
                onClick = {})
        }
    }
}

@Composable
fun AboutUI(context: Context) {
    CustomTextView(
        textData = context.getString(R.string.about_text),
        fontSize = 16,
        textColor = colorResource(id = R.color.white),
        fontFamily = FontFamily(Font(R.font.lexend_semi_bold)),
        modifier = Modifier.padding(top = 30.dp),
        textAlignment = TextAlign.Start
    )
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 20.dp)
            .background(color = colorResource(id = R.color.playlist_bg_color), shape = RoundedCornerShape(4.dp))
    ) {
        Column {
            ItemUI(context.getString(R.string.version_text), showSecondText = "8.8.76.667", showText = true, showNext = false, fontSize = 12,
                onClick = {})
            VerticalSpacer(1, colorResource(id = R.color.divider_color))
            ItemUI(context.getString(R.string.terms_and_condition_text),
                onClick = {})
            VerticalSpacer(1, colorResource(id = R.color.divider_color))
            ItemUI(context.getString(R.string.support_text),
                onClick = {})
            VerticalSpacer(1, colorResource(id = R.color.divider_color))
            ItemUI(context.getString(R.string.privacy_policy_text),
                onClick = {})
            VerticalSpacer(1, colorResource(id = R.color.divider_color))
            SimpleTextUI(context.getString(R.string.logout_text),
                onClick = {})
        }
    }
}

@Composable
fun ItemUI(
    text: String,
    showSecondText: String = "",
    showNext: Boolean = true,
    showText: Boolean = false,
    fontSize: Int = 14,
    fontFamily: FontFamily = FontFamily(Font(R.font.lexend_light)),
    onClick: (() -> Unit)
) {
    Box(
        modifier = Modifier
            .clickable { onClick() }, contentAlignment = Alignment.BottomEnd
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .height(40.dp)
                .padding(horizontal = 14.dp), horizontalArrangement = Arrangement.SpaceBetween, verticalAlignment = Alignment.CenterVertically
        ) {
            CustomTextView(
                textData = text,
                fontSize = fontSize,
                textColor = colorResource(id = R.color.white),
                fontFamily = fontFamily,
                modifier = Modifier,
                textAlignment = TextAlign.Start
            )
            Row {
                if (showText) {
                    CustomTextView(
                        textData = showSecondText,
                        fontSize = 10,
                        textColor = colorResource(id = R.color.gray_color),
                        fontFamily = fontFamily,
                        modifier = Modifier.padding(end = 10.dp),
                        textAlignment = TextAlign.Start
                    )
                }
                if (showNext)
                    Image(
                        painter = painterResource(id = R.drawable.ic_next), contentDescription = "nextImage", modifier = Modifier
                            .width(7.dp)
                            .height(12.dp)
                    )

            }
        }

    }
}

@Composable
fun SimpleTextUI(text: String, fontFamily: FontFamily = FontFamily(Font(R.font.lexend_light)), onClick: (() -> Unit)) {
    Box(
        modifier = Modifier.height(40.dp)
            .padding(horizontal = 14.dp)
            .clickable { onClick() }, contentAlignment = Alignment.Center
    ) {
        CustomTextView(
            textData = text,
            fontSize = 14,
            textColor = colorResource(id = R.color.light_blue_color),
            fontFamily = fontFamily,
            modifier = Modifier,
            textAlignment = TextAlign.Start
        )
    }
}
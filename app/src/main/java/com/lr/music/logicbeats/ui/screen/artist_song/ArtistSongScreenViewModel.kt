package com.lr.music.logicbeats.ui.screen.artist_song

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.lifecycle.ViewModel
import com.lr.music.logicbeats.model.artist_song.ArtistSongModel

class ArtistSongScreenViewModel : ViewModel() {
    val singerImage = "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg"
    val artistAlbumName = "Darshan Raval Album"
    val artistTitleName = "Darshan Raval Songs"
    var isShuffleSelected by mutableStateOf(false)
    var isRepeatSelected by mutableStateOf(false)
    var artistSongArray: SnapshotStateList<ArtistSongModel> = getArtistData()

}

fun getArtistData(): SnapshotStateList<ArtistSongModel> {
    return mutableStateListOf(
        ArtistSongModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "02:05",
            "Lorem1 Ispum is Simply",
            "Kumar Sanu, Alka Yagni",
        ),
        ArtistSongModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "02:05",
            "Lorem1 Ispum is Simply",
            "Kumar Sanu, Alka Yagni"
        ),
        ArtistSongModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "02:05",
            "Lorem1 Ispum is Simply",
            "Kumar Sanu, Alka Yagni"
        ),
        ArtistSongModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "02:05",
            "Lorem1 Ispum is Simply",
            "Kumar Sanu, Alka Yagni"
        ),
        ArtistSongModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "02:05",
            "Lorem1 Ispum is Simply",
            "Kumar Sanu, Alka Yagni"
        ),
        ArtistSongModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "02:05",
            "Lorem1 Ispum is Simply",
            "Kumar Sanu, Alka Yagni"
        ),
        ArtistSongModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "02:05",
            "Lorem1 Ispum is Simply",
            "Kumar Sanu, Alka Yagni"
        ),
        ArtistSongModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "02:05",
            "Lorem1 Ispum is Simply",
            "Kumar Sanu, Alka Yagni"
        ),
        ArtistSongModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "02:05",
            "Lorem1 Ispum is Simply",
            "Kumar Sanu, Alka Yagni"
        ),
        ArtistSongModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "02:05",
            "Lorem1 Ispum is Simply",
            "Kumar Sanu, Alka Yagni"
        )
    )
}

package com.lr.music.logicbeats.ui.screen

import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.consumeAllChanges
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.lr.music.logicbeats.R
import com.lr.music.logicbeats.common.CustomTextView
import com.lr.music.logicbeats.common.VerticalSpacer
import com.lr.music.logicbeats.navigation.RoutePath

@Composable
fun ExploreMoreScreen(navController: NavHostController) {
    var context = LocalContext.current
    var direction by remember { mutableStateOf(-1) }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Black),
        verticalArrangement = Arrangement.SpaceBetween,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth(), horizontalAlignment = Alignment.CenterHorizontally
        ) {
            VerticalSpacer(28, Color.Transparent)
            Box(
                modifier = Modifier
                    .width(363.dp)
                    .height(531.dp), contentAlignment = Alignment.Center
            ) {
                Image(
                    painter = painterResource(id = R.drawable.ic_explore),
                    contentDescription = "logo2",
                    modifier = Modifier
                        .fillMaxWidth(), contentScale = ContentScale.Crop
                )
            }
        }

        Box(modifier = Modifier
            .fillMaxSize()
            .weight(1f)
            .pointerInput(Unit) {
                detectDragGestures(
                    onDrag = { change, dragAmount ->
                        change.consumeAllChanges()

                        val (x, y) = dragAmount
                        if (Math.abs(x) > Math.abs(y)) {
                            when {
                                x > 0 -> {
                                    Log.d("TAG", "ExploreMoreScreen:X $x")
                                    //right
                                    direction = 0
                                }

                                x < 0 -> {
                                    Log.d("TAG", "ExploreMoreScreen:X12 $x")
                                    // left
                                    direction = 1
                                }
                            }
                        } else {
                            when {
                                y > 0 -> {
                                    // down
                                    Log.d("TAG", "ExploreMoreScreen:Y $x")
                                    direction = 2
                                }

                                y < 0 -> {
                                    // up
                                    Log.d("TAG", "ExploreMoreScreen:Y12 $x")
                                    direction = 3
                                }
                            }
                        }

                    },
                    onDragEnd = {
                        Log.d("TAG", "ExploreMoreScreen: Call Here  $direction")
                        when (direction) {
                            0 -> {
                                print("TAG123 Right")
                                //right swipe code here
                            }

                            1 -> {
                                print("TAG123 Left")
                                // left swipe code here
                            }

                            2 -> {
                                print("TAG123 Down")
                                // down swipe code here
                            }

                            3 -> {
                                navController.navigate(RoutePath.LoginScreen.name)
                                Log.d("TAG123", "ExploreMoreScreen: UP Call")
                                print("TAG123 UP")
                                // up swipe code here
                            }
                        }
                    }
                )
            }) {
            Box(modifier = Modifier, contentAlignment = Alignment.BottomCenter) {
                Image(
                    modifier = Modifier
                        .fillMaxSize(),
                    painter = painterResource(id = R.drawable.ic_explore_eclipse),
                    contentDescription = "bottom_image",
                    contentScale = ContentScale.FillHeight
                )
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(bottom = 40.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Image(painter = painterResource(id = R.drawable.ic_up_arrow), contentDescription = "up_image")
                    CustomTextView(
                        textData = context.getString(R.string.explore_more_text),
                        fontSize = 18,
                        fontFamily = FontFamily(Font(R.font.lexend_medium)),
                        modifier = Modifier,
                        textAlignment = TextAlign.Start
                    )
                }
            }
            CustomTextView(
                textData = context.getString(R.string.find_perfect_text),
                fontSize = 36,
                lineHeight = 44,
                fontFamily = FontFamily(Font(R.font.lexend_bold)),
                modifier = Modifier
                    .fillMaxWidth()
                    .offset(y = (-15).dp)
                    .padding(start = 25.dp),
                textAlignment = TextAlign.Start
            )
        }
    }
}
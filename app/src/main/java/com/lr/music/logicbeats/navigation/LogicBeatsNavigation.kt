package com.lr.music.logicbeats.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.lr.music.logicbeats.ui.screen.ExploreMoreScreen
import com.lr.music.logicbeats.ui.screen.HomeMainScreen
import com.lr.music.logicbeats.ui.screen.LoginScreen
import com.lr.music.logicbeats.ui.screen.artist_selection.ArtistSelection
import com.lr.music.logicbeats.ui.screen.artist_song.ArtistSongScreen
import com.lr.music.logicbeats.ui.screen.create_new_playlist.CreateNewPlaylistScreen
import com.lr.music.logicbeats.ui.screen.edit_profile.EditProfileScreen
import com.lr.music.logicbeats.ui.screen.liked_screen.LikedSongScreen
import com.lr.music.logicbeats.ui.screen.music_language.MusicLanguageScreen
import com.lr.music.logicbeats.ui.screen.notification_screen.NotificationScreen
import com.lr.music.logicbeats.ui.screen.now_playing.NowPlayingScreen
import com.lr.music.logicbeats.ui.screen.playlist_details.PlayListDetailsScreen
import com.lr.music.logicbeats.ui.screen.playlist_screen.PlayListScreen
import com.lr.music.logicbeats.ui.screen.search_screen.SearchScreen
import com.lr.music.logicbeats.ui.screen.setting.SettingScreen
import com.lr.music.logicbeats.ui.screen.sleep_timer.SleepTimer
import com.lr.music.logicbeats.ui.screen.song_details.SongDetailsScreen
import com.lr.music.logicbeats.ui.screen.splash_screen.SplashScreen

@Composable
fun LogicBeatsNavigation() {
    val navController = rememberNavController()

    NavHost(
        navController = navController,
        startDestination = RoutePath.SplashScreen.name
    ) {
        composable(RoutePath.SplashScreen.name) { SplashScreen(navController) }
        composable(RoutePath.ExploreMoreScreen.name) { ExploreMoreScreen(navController) }
        composable(RoutePath.LoginScreen.name) { LoginScreen(navController) }
        composable(RoutePath.HomeMainScreen.name) { HomeMainScreen(navController) }
        composable(RoutePath.SearchScreen.name) { SearchScreen(navController) }
        composable(RoutePath.NotificationScreen.name) { NotificationScreen(navController) }
        composable(RoutePath.SettingScreen.name) { SettingScreen(navController) }
        composable(RoutePath.EditProfileScreen.name) { EditProfileScreen(navController) }
        composable(RoutePath.MusicLanguageScreen.name) { MusicLanguageScreen(navController) }
        composable(RoutePath.ArtistSelection.name) { ArtistSelection(navController) }
        composable(RoutePath.SleepTimer.name) { SleepTimer(navController) }
        composable(RoutePath.NowPlayingScreen.name) { NowPlayingScreen(navController) }
        composable(RoutePath.ArtistSongScreen.name) { ArtistSongScreen(navController) }
        composable(RoutePath.SongDetailsScreen.name) { SongDetailsScreen(navController) }
        composable(RoutePath.CreateNewPlaylistScreen.name) { CreateNewPlaylistScreen(navController) }
        composable(RoutePath.PlayListScreen.name) { PlayListScreen(navController) }
        composable(RoutePath.PlayListDetailsScreen.name) { PlayListDetailsScreen(navController) }
        composable(RoutePath.LikedSongScreen.name) { LikedSongScreen(navController) }
    }
}
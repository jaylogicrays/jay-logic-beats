package com.lr.music.logicbeats.model.notification

data class NotificationCategoryModel(val notificationCategory : String, var isSelected : Boolean)

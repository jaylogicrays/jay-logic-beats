package com.lr.music.logicbeats.model.artist_selection

import com.lr.music.logicbeats.model.home_screen.DiscoverTitleDataModel

data class ArtistSelectionData(val languageTitle : String, val languageTitleDataArray : ArrayList<ArtistLanguageArray>, var isSelected : Boolean)
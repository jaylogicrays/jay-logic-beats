package com.lr.music.logicbeats.model.artist_selection

data class ArtistCategoryModel(val categoryName : String, var isSelected : Boolean)

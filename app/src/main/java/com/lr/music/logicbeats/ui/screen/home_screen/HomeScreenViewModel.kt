package com.lr.music.logicbeats.ui.screen.home_screen

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.lifecycle.ViewModel
import com.lr.music.logicbeats.model.home_screen.DiscoverMusicModel
import com.lr.music.logicbeats.model.home_screen.DiscoverTitleDataModel
import com.lr.music.logicbeats.model.home_screen.MadeForYouModel
import com.lr.music.logicbeats.model.home_screen.TrendingAlbumModel
import com.lr.music.logicbeats.model.home_screen.TrendingArtistModel

class HomeScreenViewModel : ViewModel() {
    var userName = "Chirag"
    var profileImage = "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg"
    var textField = mutableStateOf("")
    var trendingArtistArray = addAllSongData()
    var trendingAlbumArray = addAllAlbumData()
    var madeForYouArray = addMadeForYouData()
    var discoverTitleArray : SnapshotStateList<DiscoverMusicModel> = addDiscoverTitleArray()
    var userPlaylistArray = addUserPlaylistData()
    var discoverSongCount = 87
    var discoverSongSelectedIndex by mutableStateOf(0)
}

fun addAllSongData(): ArrayList<TrendingArtistModel> {
    return arrayListOf(
        TrendingArtistModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Honey Singh",
        ),
        TrendingArtistModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Badshah",
        ), TrendingArtistModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Badshah",
        ), TrendingArtistModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Badshah",
        ), TrendingArtistModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Badshah",
        ), TrendingArtistModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Badshah",
        )
    )
}

fun addAllAlbumData(): ArrayList<TrendingAlbumModel> {
    return arrayListOf(
        TrendingAlbumModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Desi Kalakar",
            "Honey Singh",
        ),
        TrendingAlbumModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Desi Kalakar",
            "Honey Singh",
        ),
        TrendingAlbumModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Desi Kalakar",
            "Honey Singh",
        ),
        TrendingAlbumModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Desi Kalakar",
            "Honey Singh",
        ),
        TrendingAlbumModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Desi Kalakar",
            "Honey Singh",
        ),
        TrendingAlbumModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Desi Kalakar",
            "Honey Singh",
        ),
    )
}

fun addMadeForYouData(): ArrayList<MadeForYouModel> {
    return arrayListOf(
        MadeForYouModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Labyrinth",
            "Loredana",
        ),
        MadeForYouModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Top 50",
            "Global",
        ),
        MadeForYouModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Labyrinth",
            "Loredana",
        ),
        MadeForYouModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Top 50",
            "Global",
        ),
        MadeForYouModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Labyrinth",
            "Loredana",
        ),
        MadeForYouModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Top 50",
            "Global",
        ),
        MadeForYouModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Labyrinth",
            "Loredana",
        ),
        MadeForYouModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Top 50",
            "Global",
        ),
    )
}

fun addUserPlaylistData(): ArrayList<MadeForYouModel> {
    return arrayListOf(
        MadeForYouModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Labyrinth",
            "Loredana",
        ),
        MadeForYouModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Top 50",
            "Global",
        ),
        MadeForYouModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Labyrinth",
            "Loredana",
        ),
        MadeForYouModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Top 50",
            "Global",
        ),
        MadeForYouModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Labyrinth",
            "Loredana",
        ),
        MadeForYouModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Top 50",
            "Global",
        ),
        MadeForYouModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Labyrinth",
            "Loredana",
        ),
        MadeForYouModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Top 50",
            "Global",
        ),
    )
}

fun addDiscoverTitleArray(): SnapshotStateList<DiscoverMusicModel> {
    return mutableStateListOf(
        DiscoverMusicModel(
            discoverTitle = "Song",
            discoverTitleDataArray = arrayListOf(
                DiscoverTitleDataModel(
                    "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
                    "02:05",
                    "Lorem Ispum is Simply",
                    "Kumar Sanu, Alka Yagni", isLiked = false, isPlaying = false
                ),
                DiscoverTitleDataModel(
                    "https://www.gstatic.com/webp/gallery3/1.sm.png",
                    "02:05",
                    "Lorem Ispum is Simply",
                    "Kumar Sanu, Alka Yagni", isLiked = false, isPlaying = false
                ),DiscoverTitleDataModel(
                    "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
                    "02:05",
                    "Lorem Ispum is Simply",
                    "Kumar Sanu, Alka Yagni", isLiked = false, isPlaying = false
                ),
                DiscoverTitleDataModel(
                    "https://www.gstatic.com/webp/gallery3/1.sm.png",
                    "02:05",
                    "Lorem Ispum is Simply",
                    "Kumar Sanu, Alka Yagni", isLiked = false, isPlaying = false
                ),
                DiscoverTitleDataModel(
                    "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
                    "02:05",
                    "Lorem Ispum is Simply",
                    "Kumar Sanu, Alka Yagni", isLiked = false, isPlaying = false
                ),
                DiscoverTitleDataModel(
                    "https://www.gstatic.com/webp/gallery3/1.sm.png",
                    "02:05",
                    "Lorem Ispum is Simply",
                    "Kumar Sanu, Alka Yagni", isLiked = false, isPlaying = false
                ),DiscoverTitleDataModel(
                    "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
                    "02:05",
                    "Lorem Ispum is Simply",
                    "Kumar Sanu, Alka Yagni", isLiked = false, isPlaying = false
                ),
                DiscoverTitleDataModel(
                    "https://www.gstatic.com/webp/gallery3/1.sm.png",
                    "02:05",
                    "Lorem Ispum is Simply",
                    "Kumar Sanu, Alka Yagni", isLiked = false, isPlaying = false
                )
            ),true
        ),
        DiscoverMusicModel(
            discoverTitle = "Artist",
            discoverTitleDataArray = arrayListOf(
                DiscoverTitleDataModel(
                    "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
                    "02:05",
                    "Lorem Ispum is Simply",
                    "Kumar Sanu, Alka Yagni", isLiked = true, isPlaying = false
                ),
                DiscoverTitleDataModel(
                    "https://www.gstatic.com/webp/gallery3/1.sm.png",
                    "02:05",
                    "Lorem Ispum is Simply",
                    "Kumar Sanu, Alka Yagni", isLiked = true, isPlaying = false
                ),DiscoverTitleDataModel(
                    "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
                    "02:05",
                    "Lorem Ispum is Simply",
                    "Kumar Sanu, Alka Yagni", isLiked = false, isPlaying = false
                ),
                DiscoverTitleDataModel(
                    "https://www.gstatic.com/webp/gallery3/1.sm.png",
                    "02:05",
                    "Lorem Ispum is Simply",
                    "Kumar Sanu, Alka Yagni", isLiked = false, isPlaying = false
                ),
                DiscoverTitleDataModel(
                    "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
                    "02:05",
                    "Lorem Ispum is Simply",
                    "Kumar Sanu, Alka Yagni", isLiked = false, isPlaying = false
                ),
                DiscoverTitleDataModel(
                    "https://www.gstatic.com/webp/gallery3/1.sm.png",
                    "02:05",
                    "Lorem Ispum is Simply",
                    "Kumar Sanu, Alka Yagni", isLiked = false, isPlaying = false
                ),DiscoverTitleDataModel(
                    "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
                    "02:05",
                    "Lorem Ispum is Simply",
                    "Kumar Sanu, Alka Yagni", isLiked = false, isPlaying = false
                ),
                DiscoverTitleDataModel(
                    "https://www.gstatic.com/webp/gallery3/1.sm.png",
                    "02:05",
                    "Lorem Ispum is Simply",
                    "Kumar Sanu, Alka Yagni", isLiked = false, isPlaying = false
                )
            ),false
        ),
        DiscoverMusicModel(
            discoverTitle = "Album",
            discoverTitleDataArray = arrayListOf(
                DiscoverTitleDataModel(
                    "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
                    "02:05",
                    "Lorem Ispum is Simply",
                    "Kumar Sanu, Alka Yagni", isLiked = false, isPlaying = false
                ),
                DiscoverTitleDataModel(
                    "https://www.gstatic.com/webp/gallery3/1.sm.png",
                    "02:05",
                    "Lorem Ispum is Simply",
                    "Kumar Sanu, Alka Yagni", isLiked = false, isPlaying = false
                ),DiscoverTitleDataModel(
                    "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
                    "02:05",
                    "Lorem Ispum is Simply",
                    "Kumar Sanu, Alka Yagni", isLiked = true, isPlaying = false
                ),
                DiscoverTitleDataModel(
                    "https://www.gstatic.com/webp/gallery3/1.sm.png",
                    "02:05",
                    "Lorem Ispum is Simply",
                    "Kumar Sanu, Alka Yagni", isLiked = false, isPlaying = false
                ),
                DiscoverTitleDataModel(
                    "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
                    "02:05",
                    "Lorem Ispum is Simply",
                    "Kumar Sanu, Alka Yagni", isLiked = true, isPlaying = false
                ),
                DiscoverTitleDataModel(
                    "https://www.gstatic.com/webp/gallery3/1.sm.png",
                    "02:05",
                    "Lorem Ispum is Simply",
                    "Kumar Sanu, Alka Yagni", isLiked = false, isPlaying = false
                ),DiscoverTitleDataModel(
                    "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
                    "02:05",
                    "Lorem Ispum is Simply",
                    "Kumar Sanu, Alka Yagni", isLiked = false, isPlaying = false
                ),
                DiscoverTitleDataModel(
                    "https://www.gstatic.com/webp/gallery3/1.sm.png",
                    "02:05",
                    "Lorem Ispum is Simply",
                    "Kumar Sanu, Alka Yagni", isLiked = false, isPlaying = false
                )
            ),false
        ),
        DiscoverMusicModel(
            discoverTitle = "Podcasts",
            discoverTitleDataArray = arrayListOf(
//                245,36,220,45,21,12
                DiscoverTitleDataModel(
                    "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
                    "02:05",
                    "Lorem Ispum is Simply",
                    "Kumar Sanu, Alka Yagni", isLiked = false, isPlaying = false
                ),
                DiscoverTitleDataModel(
                    "https://www.gstatic.com/webp/gallery3/1.sm.png",
                    "02:05",
                    "Lorem Ispum is Simply",
                    "Kumar Sanu, Alka Yagni", isLiked = false, isPlaying = false
                ),DiscoverTitleDataModel(
                    "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
                    "02:05",
                    "Lorem Ispum is Simply",
                    "Kumar Sanu, Alka Yagni", isLiked = false, isPlaying = false
                ),
                DiscoverTitleDataModel(
                    "https://www.gstatic.com/webp/gallery3/1.sm.png",
                    "02:05",
                    "Lorem Ispum is Simply",
                    "Kumar Sanu, Alka Yagni", isLiked = false, isPlaying = false
                ),
                DiscoverTitleDataModel(
                    "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
                    "02:05",
                    "Lorem Ispum is Simply",
                    "Kumar Sanu, Alka Yagni", isLiked = false, isPlaying = false
                ),
                DiscoverTitleDataModel(
                    "https://www.gstatic.com/webp/gallery3/1.sm.png",
                    "02:05",
                    "Lorem Ispum is Simply",
                    "Kumar Sanu, Alka Yagni", isLiked = false, isPlaying = false
                ),DiscoverTitleDataModel(
                    "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
                    "02:05",
                    "Lorem Ispum is Simply",
                    "Kumar Sanu, Alka Yagni", isLiked = false, isPlaying = false
                ),
                DiscoverTitleDataModel(
                    "https://www.gstatic.com/webp/gallery3/1.sm.png",
                    "02:05",
                    "Lorem Ispum is Simply",
                    "Kumar Sanu, Alka Yagni", isLiked = false, isPlaying = false
                )
            ),false
        )
    )
}
package com.lr.music.logicbeats.ui.screen.notification_screen

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.lifecycle.ViewModel
import com.lr.music.logicbeats.model.notification.NotificationCategoryModel
import com.lr.music.logicbeats.model.notification.NotificationTitleDataModel

class NotificationScreenViewModel : ViewModel() {
    var notificationTitleArray : SnapshotStateList<NotificationCategoryModel> = mutableStateListOf(
        NotificationCategoryModel("Songs",false),
        NotificationCategoryModel("Podcasts & Shows",false)
    )
    var notificationSongArray: SnapshotStateList<NotificationTitleDataModel> = addNotificationData()
    var selectedSongIndex by mutableStateOf(-1)
}

fun addNotificationData(): SnapshotStateList<NotificationTitleDataModel> {
    return mutableStateListOf(
        NotificationTitleDataModel(
            0,
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem1 Ispum is Simply",
            "Kumar Sanu, Alka Yagni",
            "Songs"
        ),
        NotificationTitleDataModel(
            1,
            "https://www.gstatic.com/webp/gallery3/1.sm.png",
            "02:05",
            "Lorem2 Ispum is Simply",
            "Kumar Sanu, Alka Yagni",
            "Songs"
        ),
        NotificationTitleDataModel(
            2,
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem3 Ispum is Simply",
            "Kumar Sanu, Alka Yagni",
            "Podcasts & Shows"
        ),
        NotificationTitleDataModel(
            3,
            "https://www.gstatic.com/webp/gallery3/1.sm.png",
            "02:05",
            "Lorem4 Ispum is Simply",
            "Kumar Sanu, Alka Yagni",
            "Podcasts & Shows"
        ),
        NotificationTitleDataModel(
            4,
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem5 Ispum is Simply",
            "Kumar Sanu, Alka Yagni",
            "Songs"
        ),
        NotificationTitleDataModel(
            5,
            "https://www.gstatic.com/webp/gallery3/1.sm.png",
            "02:05",
            "Lorem6 Ispum is Simply",
            "Kumar Sanu, Alka Yagni",
            "Songs"
        ),
        NotificationTitleDataModel(
            6,
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem Ispum is Simply",
            "Kumar Sanu, Alka Yagni",
            "Podcasts & Shows"
        ),
        NotificationTitleDataModel(
            7,
            "https://www.gstatic.com/webp/gallery3/1.sm.png",
            "02:05",
            "Lorem Ispum is Simply",
            "Kumar Sanu, Alka Yagni",
            "Podcasts & Shows"
        ),
        NotificationTitleDataModel(
            8,
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem Ispum is Simply",
            "Kumar Sanu, Alka Yagni",
            "Songs"
        ),
        NotificationTitleDataModel(
            9,
            "https://www.gstatic.com/webp/gallery3/1.sm.png",
            "02:05",
            "Lorem Ispum is Simply",
            "Kumar Sanu, Alka Yagni",
            "Songs"
        ),
        NotificationTitleDataModel(
            10,
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem Ispum is Simply",
            "Kumar Sanu, Alka Yagni",
            "Podcasts & Shows"
        ),
        NotificationTitleDataModel(
            11,
            "https://www.gstatic.com/webp/gallery3/1.sm.png",
            "02:05",
            "Lorem Ispum is Simply",
            "Kumar Sanu, Alka Yagni",
            "Podcasts & Shows"
        ),
        NotificationTitleDataModel(
            12,
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem Ispum is Simply",
            "Kumar Sanu, Alka Yagni",
            "Songs"
        ),
        NotificationTitleDataModel(
            13,
            "https://www.gstatic.com/webp/gallery3/1.sm.png",
            "02:05",
            "Lorem Ispum is Simply",
            "Kumar Sanu, Alka Yagni",
            "Songs"
        ),
        NotificationTitleDataModel(
            14,
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem Ispum is Simply",
            "Kumar Sanu, Alka Yagni",
            "Podcasts & Shows"
        ),
        NotificationTitleDataModel(
            15,
            "https://www.gstatic.com/webp/gallery3/1.sm.png",
            "02:05",
            "Lorem Ispum is Simply",
            "Kumar Sanu, Alka Yagni",
            "Podcasts & Shows"
        ),
    )
}
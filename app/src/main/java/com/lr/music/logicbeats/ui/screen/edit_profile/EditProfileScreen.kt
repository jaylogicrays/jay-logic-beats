package com.lr.music.logicbeats.ui.screen.edit_profile

import android.content.Context
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import com.lr.music.logicbeats.R
import com.lr.music.logicbeats.common.CustomEditTextView
import com.lr.music.logicbeats.common.CustomTextView
import com.lr.music.logicbeats.common.ProfileWithEdit
import com.lr.music.logicbeats.common.TopTitleWithCenterText
import com.lr.music.logicbeats.common.VerticalSpacer

@Composable
fun EditProfileScreen(navController: NavHostController, editProfileViewModel: EditProfileViewModel = viewModel()) {
    var context = LocalContext.current
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(colorResource(id = R.color.black))
    ) {
        TitleBarUI(navController = navController, context = context)
        Column(modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 14.dp)
            .verticalScroll(rememberScrollState())) {
            VerticalSpacer(34, Color.Transparent)
            ProfileWithEdit(onClick = {
            })
            ProfileUI(editProfileViewModel,context)
        }
//        NotificationTitleUI(notificationScreenViewModel, context)
    }
}

@Composable
fun TitleBarUI(navController: NavController, context: Context) {
    Row(modifier = Modifier.fillMaxWidth()) {
        TopTitleWithCenterText(centerTitle = context.getString(R.string.edit_profile_text), showMenuButton = true, isImage = false) {
            navController.navigateUp()
        }
    }
}

@Composable
fun ProfileUI(editProfileViewModel: EditProfileViewModel, context: Context) {
    CustomTextView(
        textData = context.getString(R.string.name_text),
        fontSize = 12,
        textColor = colorResource(id = R.color.gray_color),
        fontFamily = FontFamily(Font(R.font.lexend_regular)),
        modifier = Modifier.padding(top = 36.dp),
        textAlignment = TextAlign.Start
    )
    CustomEditTextView(
        modifier = Modifier
            .fillMaxWidth().padding(top = 8.dp)
            .height(50.dp),
        textField = editProfileViewModel.nameTextField,
        hintsString = context.getString(R.string.enter_name_hints),
        onValueChanged = {
            editProfileViewModel.nameTextField = it
        },
        backgroundColor = colorResource(id = R.color.playlist_bg_color)
    )
    CustomTextView(
        textData = context.getString(R.string.email_id_text),
        fontSize = 12,
        textColor = colorResource(id = R.color.gray_color),
        fontFamily = FontFamily(Font(R.font.lexend_regular)),
        modifier = Modifier.padding(top = 16.dp),
        textAlignment = TextAlign.Start
    )
    CustomEditTextView(
        modifier = Modifier
            .fillMaxWidth().padding(top = 8.dp)
            .height(50.dp),
        textField = editProfileViewModel.emailTextField,
        hintsString = context.getString(R.string.email_id_hints),
        onValueChanged = {
            editProfileViewModel.emailTextField = it
        },
        backgroundColor = colorResource(id = R.color.playlist_bg_color)
    )
    CustomTextView(
        textData = context.getString(R.string.mobile_number_text),
        fontSize = 12,
        textColor = colorResource(id = R.color.gray_color),
        fontFamily = FontFamily(Font(R.font.lexend_regular)),
        modifier = Modifier.padding(top = 16.dp),
        textAlignment = TextAlign.Start
    )
    CustomEditTextView(
        modifier = Modifier
            .fillMaxWidth().padding(top = 8.dp)
            .height(50.dp),
        textField = editProfileViewModel.mobileNumberTextField,
        hintsString = context.getString(R.string.mobile_number_hints),
        onValueChanged = {
            editProfileViewModel.mobileNumberTextField = it
        },
        backgroundColor = colorResource(id = R.color.playlist_bg_color)
    )
    CustomTextView(
        textData = context.getString(R.string.age_text),
        fontSize = 12,
        textColor = colorResource(id = R.color.gray_color),
        fontFamily = FontFamily(Font(R.font.lexend_regular)),
        modifier = Modifier.padding(top = 16.dp),
        textAlignment = TextAlign.Start
    )
    CustomEditTextView(
        modifier = Modifier
            .fillMaxWidth().padding(top = 8.dp)
            .height(50.dp),
        textField = editProfileViewModel.ageTextField,
        hintsString = context.getString(R.string.age_hints),
        onValueChanged = {
            editProfileViewModel.ageTextField = it
        },
        backgroundColor = colorResource(id = R.color.playlist_bg_color)
    )
    CustomTextView(
        textData = context.getString(R.string.gender_text),
        fontSize = 12,
        textColor = colorResource(id = R.color.gray_color),
        fontFamily = FontFamily(Font(R.font.lexend_regular)),
        modifier = Modifier.padding(top = 16.dp),
        textAlignment = TextAlign.Start
    )
    CustomEditTextView(
        modifier = Modifier
            .fillMaxWidth().padding(top = 8.dp)
            .height(50.dp),
        textField = editProfileViewModel.genderTextField,
        hintsString = context.getString(R.string.gender_hints),
        onValueChanged = {
            editProfileViewModel.genderTextField = it
        },
        isEdit = false,
        isArrow = true,
        backgroundColor = colorResource(id = R.color.playlist_bg_color)
    )
}
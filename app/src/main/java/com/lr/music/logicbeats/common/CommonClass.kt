package com.lr.music.logicbeats.common

import android.util.Log
import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.tween
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonColors
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.Text
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImagePainter
import coil.compose.rememberAsyncImagePainter
import com.lr.music.logicbeats.R
import com.lr.music.logicbeats.ui.screen.create_new_playlist.CreateNewPlaylistViewModel
import kotlinx.coroutines.delay

@Composable
fun VerticalSpacer(
    height: Int = 5,
    backgroundColor: Color = Color.White,
    horizontalPadding: Int = 0
) {
    Spacer(
        modifier = Modifier
            .height(height.dp)
            .fillMaxWidth()
            .padding(horizontal = horizontalPadding.dp)
            .background(backgroundColor)
    )
}

@Composable
fun HorizontalSpacer(
    width: Int = 5,
    backgroundColor: Color = Color.White,
    horizontalPadding: Int = 0
) {
    Spacer(
        modifier = Modifier
            .width(width.dp)
            .padding(horizontal = horizontalPadding.dp)
            .background(backgroundColor)
    )
}

@Composable
fun CustomTextView(
    modifier: Modifier = Modifier.fillMaxWidth(),
    textData: String,
    fontSize: Int = 22,
    textColor: Color = colorResource(id = R.color.white),
    lineHeight: Int = 20,
    maxLines: Int = Int.MAX_VALUE,
    textAlignment: TextAlign = TextAlign.Center,
    fontFamily: FontFamily = FontFamily(Font(R.font.lexend_medium))
) {
    Log.d("TAG", "CustomTextView:12 $textData")
    Text(
        text = textData,
        modifier = modifier,
        style = TextStyle(
            textAlign = textAlignment,
            fontSize = fontSize.sp,
            lineHeight = lineHeight.sp,
            color = textColor,
        ), fontFamily = fontFamily, maxLines = maxLines,
        overflow = TextOverflow.Ellipsis
    )
}

@Composable
fun CustomTextField(
    modifier: Modifier,
    hintText: String = "",
    labelText: String = hintText,
    textFieldValue: String,
    onValueChanged: (text: String) -> Unit,
    isEnable: Boolean = true,
    isError: Boolean = false,
    errorText: String = "",
    keyboardInputType: KeyboardType = KeyboardType.Text,
    imeAction: ImeAction = ImeAction.Next,
    singleLine: Boolean = true,
    maxLines: Int = 1,
    minLines: Int = 1,
    shape: Shape = MaterialTheme.shapes.extraLarge
) {
    OutlinedTextField(
        value = textFieldValue,
        onValueChange = { onValueChanged(it) },
        modifier = modifier.fillMaxWidth(),
        enabled = isEnable,
        textStyle = TextStyle(
            fontFamily = FontFamily(Font(R.font.lexend_light)),
            fontWeight = FontWeight(400),
            fontSize = 14.sp,
            color = Color.White
        ),
        label = {
            Text(
                text = labelText,
                style = TextStyle(
                    fontFamily = FontFamily(Font(R.font.lexend_light)),
                    fontWeight = FontWeight(700),
                    fontSize = 12.sp,
                    color = Color.White
                )
            )
        },
        placeholder = {
            Text(
                text = hintText,
                style = TextStyle(
                    fontFamily = FontFamily(Font(R.font.lexend_light)),
                    fontWeight = FontWeight(400),
                    fontSize = 14.sp,
                    color = Color.White
                )
            )
        },
        isError = errorText.isNotEmpty(),
        supportingText = {
            Text(
                text = errorText, style = TextStyle(
                    fontFamily = FontFamily(Font(R.font.lexend_light)),
                    fontWeight = FontWeight(400),
                    fontSize = 12.sp,
                )
            )
        },
        keyboardOptions = KeyboardOptions(keyboardType = keyboardInputType, imeAction = imeAction),
        singleLine = singleLine,
        maxLines = maxLines,
        minLines = minLines,
        shape = shape,
        colors = OutlinedTextFieldDefaults.colors(
            focusedTextColor = colorResource(id = R.color.container_color),
            cursorColor = colorResource(id = R.color.container_color),
            focusedBorderColor = colorResource(id = R.color.container_color),
            unfocusedBorderColor = colorResource(id = R.color.container_color),
            unfocusedContainerColor = colorResource(id = R.color.container_color),
            focusedContainerColor = colorResource(id = R.color.container_color),
        )
    )
}

@Composable
fun CustomButton(
    modifier: Modifier = Modifier,
    text: String,
    backgroundColor: ButtonColors = ButtonDefaults.buttonColors(colorResource(id = R.color.light_blue_color)),
    corner: Shape = RoundedCornerShape(5.dp),
    fontSize: Int = 14,
    fontColor: Color = Color.White,
    align: TextAlign = TextAlign.Center,
    fontFamily: FontFamily = FontFamily(Font(R.font.lexend_medium)),
    showNext: Boolean = false,
    onClick: (() -> Unit),
    borderStroke: BorderStroke = BorderStroke(width = 1.dp, color = colorResource(id = R.color.light_blue_color))
) {
    Button(
        onClick = onClick,
        modifier = modifier,
        elevation = ButtonDefaults.buttonElevation(5.dp),
        shape = corner,
        contentPadding = PaddingValues(horizontal = 17.dp),
        colors = backgroundColor,
        border = borderStroke
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = text,
                textAlign = align,
                modifier = Modifier.weight(1f),
                style = TextStyle(
                    fontSize = fontSize.sp,
                    color = fontColor,
                    fontFamily = fontFamily
                ),
            )
        }
    }
}

@Composable
fun CustomSearchView(
    modifier: Modifier = Modifier
        .fillMaxWidth()
        .height(50.dp),
    hintsString: String,
    textField: String,
    textSize: Int = 12,
    fontFamily: FontFamily = FontFamily(Font(R.font.lexend_regular)),
    corner: Shape = RoundedCornerShape(10.dp),
    backgroundColor: Color = colorResource(id = R.color.container_color),
    icon: Int = R.drawable.ic_search,
    iconSize: Int = 18,
    onValueChanged: (text: String) -> Unit
) {
    OutlinedTextField(
        value = textField,
        onValueChange = { onValueChanged(it) },
        shape = corner,
        modifier = modifier,
        placeholder = {
            Text(
                text = hintsString,
                style = TextStyle(fontSize = 12.sp, color = colorResource(id = R.color.white), fontFamily = fontFamily),
                modifier = Modifier.padding(0.dp)
            )
        },
        leadingIcon = {
            Icon(
                painter = painterResource(icon),
                contentDescription = "Icon",
                modifier = Modifier
                    .size(iconSize.dp)
                    .padding(0.dp),
                tint = colorResource(id = R.color.white)
            )
        },
        singleLine = true,
        textStyle = TextStyle(
            fontSize = textSize.sp,
            color = colorResource(id = R.color.white),
            fontFamily = fontFamily
        ),
        colors = TextFieldDefaults.colors(
            cursorColor = colorResource(id = R.color.white),
            focusedContainerColor = backgroundColor,
            unfocusedContainerColor = backgroundColor,
            focusedIndicatorColor = backgroundColor,
            unfocusedIndicatorColor = backgroundColor
        ),
        visualTransformation = VisualTransformation.None,
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TopTitleWithCenterText(
    showBack: Boolean = true,
    showMenuButton: Boolean = false,
    isImage: Boolean = true,
    userImage: String = "null",
    centerTitle: String? = "",
    fontSize: Int = 20,
    fontColor: Color = colorResource(id = R.color.white),
    fontFamily: FontFamily = FontFamily(Font(R.font.lexend_medium)),
    onClick: () -> Unit,
) {
    CenterAlignedTopAppBar(
        modifier = Modifier
            .fillMaxWidth()
            .height(52.dp),
        colors = TopAppBarDefaults.centerAlignedTopAppBarColors(Color.Transparent),
        title = {
            Box(modifier = Modifier.fillMaxHeight(), contentAlignment = Alignment.Center) {
                Text(
                    text = centerTitle!!,
                    style = TextStyle(
                        fontSize = fontSize.sp,
                        color = fontColor,
                        fontFamily = fontFamily,
                    ),
                )
            }
        },
        navigationIcon = {
            if (showBack)
                IconButton(
                    onClick = { onClick() }, modifier = Modifier
                        .fillMaxHeight()
                        .padding(end = 14.dp)
                        .width(40.dp)
                        .clip(RoundedCornerShape(10.dp))
                        .background(colorResource(id = R.color.container_color))
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.ic_back),
                        contentDescription = "backImage",
                        modifier = Modifier
                            .width(8.dp)
                            .height(14.dp)
                    )
                }
        },
        actions = {
            if (showMenuButton)
                IconButton(
                    onClick = { onClick() }, modifier = Modifier
                        .fillMaxHeight()
                        .width(60.dp)
                ) {
                    if (isImage)
                        Image(
                            painter = rememberAsyncImagePainter(model = userImage),
                            contentDescription = "profileImage",
                            modifier = Modifier
                                .size(40.dp)
                                .clip(CircleShape), contentScale = ContentScale.Crop
                        )
                    else
                        CustomTextView(
                            textData = LocalContext.current.getString(R.string.save_text),
                            fontSize = 17,
                            textColor = colorResource(id = R.color.light_blue_color),
                            fontFamily = FontFamily(Font(R.font.lexend_medium)),
                            modifier = Modifier,
                            textAlignment = TextAlign.Start
                        )
                }
        }
    )
}

@Composable
fun ProfileWithEdit(
    image: AsyncImagePainter = rememberAsyncImagePainter("https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg"),
    onClick: () -> Unit
) {
    Box(modifier = Modifier.fillMaxWidth(), contentAlignment = Alignment.Center) {
        Box(
            modifier = Modifier, contentAlignment = Alignment.BottomEnd
        ) {
            Image(
                painter = image, contentDescription = "Profile Image", modifier = Modifier
                    .size(167.dp)
                    .clip(CircleShape), contentScale = ContentScale.Crop
            )
            Box(
                modifier = Modifier
                    .padding(end = 19.dp)
                    .size(30.dp)
                    .clip(RoundedCornerShape(5.dp))
                    .clickable(onClick = onClick)
                    .background(color = colorResource(id = R.color.light_white_color)),
                contentAlignment = Alignment.Center
            ) {
                Image(
                    painter = painterResource(id = R.drawable.ic_edit),
                    contentDescription = "image",
                    modifier = Modifier.size(16.dp)
                )
            }
        }
    }
}

@Composable
fun CustomEditTextView(
    modifier: Modifier = Modifier
        .fillMaxWidth()
        .height(42.dp),
    hintsString: String,
    textField: String,
    textSize: Int = 12,
    enabled: Boolean = true,
    fontFamily: FontFamily = FontFamily(Font(R.font.lexend_regular)),
    corner: Shape = RoundedCornerShape(4.dp),
    isArrow: Boolean = false,
    backgroundColor: Color = colorResource(id = R.color.playlist_bg_color),
    onValueChanged: ((text: String) -> Unit), isEdit: Boolean = true,
    keyboardOptions: KeyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Text),
    keyboardActions: KeyboardActions = KeyboardActions(onDone = { }),
) {
    Card(
        modifier = modifier,
        colors = CardDefaults.cardColors(backgroundColor),
        border = BorderStroke(width = 1.dp, color = backgroundColor),
        shape = corner
    ) {
        Row(
            modifier = Modifier.fillMaxSize(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            OutlinedTextField(
                value = textField,
                onValueChange = { onValueChanged(it) },
                shape = corner,
                modifier = Modifier,
                enabled = enabled,
                placeholder = {
                    Text(
                        text = hintsString,
                        style = TextStyle(
                            fontSize = textSize.sp,
                            fontFamily = fontFamily,
                            color = colorResource(id = R.color.light_gray)
                        ),
                        modifier = Modifier.padding(0.dp)
                    )
                },
                singleLine = true,
                textStyle = TextStyle(
                    fontSize = textSize.sp,
                    color = colorResource(id = R.color.white),
                    fontFamily = fontFamily
                ),
                colors = TextFieldDefaults.colors(
                    cursorColor = colorResource(id = R.color.white),
                    focusedContainerColor = Color.Transparent,
                    unfocusedContainerColor = Color.Transparent,
                    focusedIndicatorColor = Color.Transparent,
                    unfocusedIndicatorColor = Color.Transparent
                ),
                keyboardOptions = keyboardOptions,
                keyboardActions = keyboardActions
            )
            if (isEdit) {
                Box(
                    modifier = Modifier
                        .size(50.dp)
                        .background(color = backgroundColor),
                    contentAlignment = Alignment.Center
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_edit),
                        contentDescription = "edit_image",
                        modifier = Modifier
                            .size(16.dp), tint = colorResource(id = R.color.gray_color)
                    )
                }
            } else if (isArrow) {
                Box(
                    modifier = Modifier
                        .size(50.dp)
                        .background(color = backgroundColor),
                    contentAlignment = Alignment.Center
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_arrow_up),
                        contentDescription = "up_arrow_image",
                        modifier = Modifier
                            .width(12.dp)
                            .height(7.dp), tint = colorResource(id = R.color.gray_color)
                    )
                }
            }
        }
    }
}

fun getTimeInFormat(timeMinutes: Long): String {
    Log.d("TAG", "getTimeInFormat: Call Here $timeMinutes")
    val hours = timeMinutes.toInt() / 60
//    var mins = timeMinutes.toInt() - hours * 60   /*9304593947*/
    return if (hours > 0) "$hours hours" else "$timeMinutes minutes"
}


@Composable
fun ArcProgressbar(
    modifier: Modifier = Modifier,
    newScore: Float,
    level: String,
    startAngle: Float = 180f,
    limitAngle: Float = 180f,
    thickness: Dp = 5.dp
) {

    val animateValue = remember { Animatable(0f) }

    LaunchedEffect(newScore) {
        if (newScore > 0f) {
            animateValue.snapTo(0f)
            delay(10)
            animateValue.animateTo(
                targetValue = limitAngle,
                animationSpec = tween(
                    durationMillis = 1000
                )
            )
        }
    }

    Box(modifier = modifier.fillMaxWidth()) {

        Canvas(
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp)
                .aspectRatio(1f),
            onDraw = {
                // Background Arc
                drawArc(
                    color = Color.Gray,
                    startAngle = startAngle,
                    sweepAngle = limitAngle,
                    useCenter = false,
                    style = Stroke(thickness.toPx(), cap = StrokeCap.Square),
                    size = Size(size.width, size.height)
                )

                // Foreground Arc
                drawArc(
                    color = Color.Green,
                    startAngle = startAngle,
                    sweepAngle = if (newScore<=180f) newScore else 180f,
                    useCenter = false,
                    style = Stroke(thickness.toPx(), cap = StrokeCap.Square),
                    size = Size(size.width, size.height)
                )
            }
        )

        /* Column {
             Text(
                 text = level,
                 modifier = Modifier
                     .fillMaxWidth(0.125f)
                     .offset(y = (-10).dp),
                 color = Color.Gray,
                 fontSize = 82.sp
             )

             Text(
                 text = "LEVEL",
                 modifier = Modifier
                     .padding(bottom = 8.dp),
                 color = Color.Gray,
                 fontSize = 20.sp
             )

             Text(
                 text = "Score ( $newScore ) ",
                 modifier = Modifier
                     .padding(bottom = 8.dp),
                 color = Color.Gray,
                 fontSize = 20.sp
             )
      }*/
    }
}
package com.lr.music.logicbeats.model.artist_song

data class ArtistSongModel(
    val songImage: String,
    val songDuration: String,
    val songName: String,
    val singerName: String
)
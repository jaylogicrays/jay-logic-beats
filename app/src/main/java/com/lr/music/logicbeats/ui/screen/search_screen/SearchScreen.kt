package com.lr.music.logicbeats.ui.screen.search_screen

import android.content.Context
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import coil.compose.rememberAsyncImagePainter
import com.lr.music.logicbeats.R
import com.lr.music.logicbeats.common.CustomSearchView
import com.lr.music.logicbeats.common.CustomTextView
import com.lr.music.logicbeats.common.VerticalSpacer
import com.lr.music.logicbeats.model.search_screen.SearchScreenModel

@Composable
fun SearchScreen(navController: NavHostController, searchScreenViewModel: SearchScreenViewModel = viewModel()) {
    val context = LocalContext.current
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(colorResource(id = R.color.black))
            .padding(horizontal = 14.dp)
    ) {
        SearchHistorySelectionUI(searchScreenViewModel, context, onClick = {navController.navigateUp()})
        CustomTextView(
            textData = context.getString(R.string.recent_searched_text),
            fontSize = 16,
            textColor = colorResource(id = R.color.white),
            fontFamily = FontFamily(Font(R.font.lexend_bold)),
            modifier = Modifier.padding(top = 36.dp),
            textAlignment = TextAlign.Start
        )
        SearchViewItem(searchScreenViewModel)
    }
}

@Composable
fun SearchHistorySelectionUI(searchViewModel: SearchScreenViewModel, context: Context, onClick: (() -> Unit)) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 18.dp)
            .height(52.dp), verticalAlignment = Alignment.CenterVertically
    ) {
        IconButton(
            onClick = { onClick() }, modifier = Modifier
                .fillMaxHeight()
                .padding(end = 14.dp)
                .width(40.dp)
                .clip(RoundedCornerShape(10.dp))
                .background(colorResource(id = R.color.container_color))
        ) {
            Image(
                painter = painterResource(id = R.drawable.ic_back),
                contentDescription = "backImage",
                modifier = Modifier
                    .width(8.dp)
                    .height(14.dp)
            )
        }
        CustomSearchView(
            modifier = Modifier
                .fillMaxWidth()
                .shadow(elevation = 5.dp, shape = RoundedCornerShape(10.dp))
                .clip(RoundedCornerShape(10.dp))
                .height(50.dp),
            hintsString = context.getString(R.string.search_hint_text),
            textField = searchViewModel.textField
        ) {
            searchViewModel.textField = it
            filterDataArray(searchViewModel, it)
        }
    }
}

@Composable
fun SearchViewItem(searchScreenViewModel: SearchScreenViewModel) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .verticalScroll(rememberScrollState())
            .padding(vertical = 5.dp)
    ) {
        VerticalSpacer(12, Color.Transparent)
        searchScreenViewModel.tempDataArray.forEachIndexed { index, searchScreenModel ->
            SearchHistoryItem(searchScreenModel, onClick = {
                val itr = searchScreenViewModel.historyDataArray.iterator()
                while (itr.hasNext()) {
                    if (itr.next().index == searchScreenModel.index) {
                        itr.remove()
                        break
                    }
                }
                searchScreenViewModel.tempDataArray.removeAt(index)
            })
        }
    }
}

@Composable
fun SearchHistoryItem(searchItem: SearchScreenModel, onClick: (() -> Unit)) {
    Column(modifier = Modifier) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 18.dp), horizontalArrangement = Arrangement.SpaceBetween, verticalAlignment = Alignment.CenterVertically
        ) {
            Row(
                modifier = Modifier, verticalAlignment = Alignment.CenterVertically
            ) {
                Image(
                    painter = rememberAsyncImagePainter(searchItem.songImage),
                    contentDescription = "Person Image",
                    modifier = Modifier
                        .size(50.dp)
                        .clip(RoundedCornerShape(10.dp)), contentScale = ContentScale.Crop
                )
                Column(modifier = Modifier.padding(start = 17.dp)) {
                    CustomTextView(
                        textData = searchItem.songName,
                        fontSize = 14,
                        textColor = colorResource(id = R.color.white),
                        fontFamily = FontFamily(Font(R.font.lexend_medium)),
                        modifier = Modifier,
                        textAlignment = TextAlign.Start
                    )
                    Row(modifier = Modifier.padding(top = 7.dp), verticalAlignment = Alignment.CenterVertically) {
                        CustomTextView(
                            textData = searchItem.songDuration,
                            fontSize = 10,
                            textColor = colorResource(id = R.color.gray_color),
                            fontFamily = FontFamily(Font(R.font.lexend_regular)),
                            modifier = Modifier,
                            textAlignment = TextAlign.Start,
                        )
                        Box(
                            modifier = Modifier
                                .padding(horizontal = 5.dp)
                                .size(3.dp)
                                .clip(CircleShape)
                                .background(colorResource(id = R.color.gray_color))
                        )
                        CustomTextView(
                            textData = searchItem.singerName,
                            fontSize = 10,
                            textColor = colorResource(id = R.color.gray_color),
                            fontFamily = FontFamily(Font(R.font.lexend_regular)),
                            modifier = Modifier,
                            textAlignment = TextAlign.Start,
                        )
                    }
                }
            }
            IconButton(onClick = { onClick() }) {
                Image(painter = painterResource(id = R.drawable.ic_closes), contentDescription = "close_image", modifier = Modifier.size(13.dp))
            }
        }
        VerticalSpacer(18, Color.Transparent)
        VerticalSpacer(1, colorResource(id = R.color.divider_color))
        /*Spacer(
            modifier = Modifier
                .padding(top = 18.dp)
                .height(1.dp)
                .fillMaxWidth()
                .background(colorResource(id = R.color.divider_color))
        )*/
    }
}

fun filterDataArray(searchScreenViewModel: SearchScreenViewModel, text: String) {
    searchScreenViewModel.tempDataArray.clear()
    searchScreenViewModel.historyDataArray.forEach {
        if (it.songName.contains(text)) {
            searchScreenViewModel.tempDataArray.add(it)
        }
    }
}
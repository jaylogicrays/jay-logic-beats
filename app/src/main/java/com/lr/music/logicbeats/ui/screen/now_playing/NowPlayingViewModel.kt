package com.lr.music.logicbeats.ui.screen.now_playing

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.lifecycle.ViewModel
import com.lr.music.logicbeats.model.now_playing.NowPlayingModel

class NowPlayingViewModel : ViewModel() {
    var pagePosition by mutableStateOf(0)
    var nowPlayingSongArray: SnapshotStateList<NowPlayingModel> = addNowPlayingSongArray()
}

fun addNowPlayingSongArray(): SnapshotStateList<NowPlayingModel> {
    return mutableStateListOf(
        NowPlayingModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3",
            "Bank Account 1",
            "21 Savage",
            false
        ),
        NowPlayingModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3",
            "Bank Account 2",
            "22 Savage",
            false
        ),
        NowPlayingModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3",
            "Bank Account 3",
            "23 Savage",
            false
        ),
        NowPlayingModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3",
            "Bank Account 4",
            "24 Savage",
            false
        ),
        NowPlayingModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3",
            "Bank Account 5",
            "25 Savage",
            false
        ),
        NowPlayingModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3",
            "Bank Account",
            "24 Savage",
            false
        ),
        NowPlayingModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3",
            "Bank Account",
            "21 Savage",
            false
        ),
        NowPlayingModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3",
            "Bank Account",
            "21 Savage",
            false
        ),
        NowPlayingModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3",
            "Bank Account",
            "21 Savage",
            false
        ),
        NowPlayingModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3",
            "Bank Account",
            "21 Savage",
            false
        ),
        NowPlayingModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3",
            "Bank Account",
            "21 Savage",
            false
        ),
        NowPlayingModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3",
            "Bank Account",
            "21 Savage",
            false
        ),
        NowPlayingModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3",
            "Bank Account",
            "21 Savage",
            false
        ),
        NowPlayingModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3",
            "Bank Account",
            "21 Savage",
            false
        ),
    )
}
package com.lr.music.logicbeats.model.artist_song

data class SingerModel(val singerImage : String, val singerName : String)
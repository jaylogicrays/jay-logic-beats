package com.lr.music.logicbeats.ui.screen.now_playing

import android.content.Context
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.util.lerp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import coil.compose.rememberAsyncImagePainter
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.calculateCurrentOffsetForPage
import com.google.accompanist.pager.rememberPagerState
import com.lr.music.logicbeats.R
import com.lr.music.logicbeats.common.ArcProgressbar
import com.lr.music.logicbeats.common.CustomTextView
import com.lr.music.logicbeats.common.TopTitleWithCenterText
import io.github.ningyuv.circularseekbar.CircularSeekbarView
import kotlin.math.absoluteValue

@Composable
fun NowPlayingScreen(navController: NavHostController, nowPlayingViewModel: NowPlayingViewModel = viewModel()) {
    var context = LocalContext.current
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(colorResource(id = R.color.black)),
        verticalArrangement = Arrangement.SpaceBetween
    ) {
        Column {
            TitleBarUI(navController = navController, context = context)
            BuildLoginSlider(nowPlayingViewModel)
            TitleUI(nowPlayingViewModel)
        }
        BottomProgressBar()
    }
}

@Composable
fun TitleBarUI(navController: NavController, context: Context) {
    Row(modifier = Modifier.fillMaxWidth()) {
        TopTitleWithCenterText(centerTitle = context.getString(R.string.now_playing)) {
            navController.navigateUp()
        }
    }
}

@Composable
fun BottomProgressBar() {
    var value by rememberSaveable {
        mutableStateOf(0f)
    }
    Box(modifier = Modifier.fillMaxWidth().offset(y = 150.dp)) {
        Column {
            Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween) {
                IconButton(onClick = { /*TODO*/ }) {
                    Image(painter = painterResource(id = R.drawable.ic_suffle), contentDescription = "suffle_image", modifier = Modifier.size(20.dp))
                }
                IconButton(onClick = { /*TODO*/ }) {
                    Image(painter = painterResource(id = R.drawable.ic_repeat), contentDescription = "suffle_image", modifier = Modifier.size(20.dp))
                }
            }
            ScoreGenerator()
            /*CircularSeekbarView(
                value = value,
                onChange = { v -> value = 5f },
                lineWeight = 1.dp,
                startAngle = -90f,
                fullAngle = 180f,
                dotRadius = 10.dp,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 14.dp, vertical = 30.dp),
            )*/
            Row(modifier = Modifier.fillMaxWidth().padding(horizontal = 14.dp).offset(y=-200.dp), horizontalArrangement = Arrangement.SpaceBetween) {
                CustomTextView(
                    textData = "00:00",
                    fontSize = 12,
                    textColor = colorResource(id = R.color.white),
                    fontFamily = FontFamily(Font(R.font.lexend_light)),
                    modifier = Modifier.padding(top = 7.dp),
                    textAlignment = TextAlign.Start
                )
                CustomTextView(
                    textData = "05:00",
                    fontSize = 12,
                    textColor = colorResource(id = R.color.white),
                    fontFamily = FontFamily(Font(R.font.lexend_light)),
                    modifier = Modifier.padding(top = 7.dp),
                    textAlignment = TextAlign.Start
                )
            }
        }
    }
}

/*@OptIn(ExperimentalFoundationApi::class)
@Composable
fun Carousel(images: List<Bitmap>, modifier: Modifier = Modifier) {
    val coroutineScope = rememberCoroutineScope()
    val pagerState = rememberPagerState(
        pageCount = images.size,
        initialPage = 0,
        initialPageOffset = 0f
    )

    HorizontalPager(
        state = pagerState,
        modifier = modifier.fillMaxSize(),
        itemSpacing = 10.dp
    ) { page ->
        val currentImage = images[page]
        val offset = (pagerState.currentPage - page) + pagerState.currentPageOffset

        Image(
            bitmap = currentImage.asImageBitmap(),
            contentDescription = null,
            modifier = Modifier.run {
                graphicsLayer {
                            lerp(
                         start = 0.85f,
                                stop = 1f,
                                fraction = kotlin.math.abs(offset).coerceIn(0f, 1f)
                            ).also { scale ->
                                scaleX = scale
                                scaleY = scale
                            }
                            // Change the alpha of the page depending on its position
                            alpha = lerp(
                                start = 0.5f,
                                stop = 1f,
                                fraction = 1f - kotlin.math.abs(offset)
                            )
                        }
                        .zIndex(1f - kotlin.math.abs(offset))
            }
        )
    }
}*/

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun BuildLoginSlider(nowPlayingViewModel: NowPlayingViewModel) {
    val pagerState = rememberPagerState(initialPage = 1)
//    val sliderList = arrayListOf(R.drawable.ic_pic,R.drawable.ic_pic,R.drawable.ic_pic,R.drawable.ic_pic,R.drawable.ic_pic,R.drawable.ic_pic,R.drawable.ic_pic, )
    /*val sliderList = arrayListOf(
        "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
        "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
        "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
        "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
        "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
        "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
    )*/


    HorizontalPager(
        count = nowPlayingViewModel.nowPlayingSongArray.size, state = pagerState,
        contentPadding = PaddingValues(horizontal = 150.dp),
        verticalAlignment = Alignment.CenterVertically,
    ) { page ->
        Card(
            colors = CardDefaults.cardColors(Color.Transparent),
            shape = RoundedCornerShape(10.dp),
            elevation = CardDefaults.cardElevation(1.dp),
            modifier = Modifier
                .graphicsLayer {
                    val pageOffset = calculateCurrentOffsetForPage(page).absoluteValue

                    // We animate the scaleX + scaleY, between 85% and 100%
                    /*lerp(
                        start = 0.95f,
                        stop = 1f,
                        fraction = 0.5f *//*- pageOffset.coerceIn(0f, 1f)*//*
                    ).also { scale ->
                        scaleX = scale
                        scaleY = scale
                    }

                    // We animate the alpha, between 50% and 100%
                    alpha = lerp(
                        start = 0.5f,
                        stop = 1f,
                        fraction = 1f - pageOffset.coerceIn(0f, 1f)
                    )
*/
//                    val pageOffset = calculateCurrentOffsetForPage(page).absoluteValue
                    lerp(
                        start = 0.65f,
                        stop = 1f,
                        fraction = 1f - pageOffset.coerceIn(0f, 1f)
                    ).also { scale ->
                        scaleX = scale
                        scaleY = scale
                    }
                    alpha = lerp(
                        start = 0.25f,
                        stop = 1f,
                        fraction = 1f - pageOffset.coerceIn(0f, 1f)
                    )
                }
                .aspectRatio(0.5f)
        ) {
            nowPlayingViewModel.pagePosition = pagerState.currentPage
            Image(
                painter = rememberAsyncImagePainter(model = nowPlayingViewModel.nowPlayingSongArray[page].songImage),
                contentDescription = "image",
                contentScale = ContentScale.Crop
            )
            //            Image(painter = rememberAsyncImagePainter(id = sliderList[page]), contentDescription = "image", contentScale = ContentScale.Fit)
            /*AsyncImage(
                model = ImageRequest.Builder(LocalContext.current)
                    .data(sliderList[page])
//                    .crossfade(true)
                    .scale(Scale.FILL)
                    .build(),
                contentDescription = null,
               *//* modifier = Modifier
                    .offset {
                        // Calculate the offset for the current page from the
                        // scroll position
                        val pageOffset =
                            calculateCurrentOffsetForPage(page)
                        // Then use it as a multiplier to apply an offset
                        IntOffset(
                            x = ((10.dp) * pageOffset).roundToPx(),
                            y = 0,
                        )
                    }*//*
            )*/
        }
        /* LaunchedEffect(pagerState) {
             snapshotFlow { pagerState.currentPage }.collect { page ->
                 TitleUI(nowPlayingModel = nowPlayingViewModel.nowPlayingSongArray[page])
             }
         }*/
    }
}
/*
@OptIn(ExperimentalFoundationApi::class)
@Composable
fun MusicSlider() {
    HorizontalPager(count = 4) { page ->
        Card(
            Modifier
                .graphicsLayer {
                    // Calculate the absolute offset for the current page from the
                    // scroll position. We use the absolute value which allows us to mirror
                    // any effects for both directions
                    val pageOffset = calculateCurrentOffsetForPage(page).absoluteValue

                    // We animate the scaleX + scaleY, between 85% and 100%
                    lerp(
                        start = 0.85f,
                        stop = 1f,
                        fraction = 1f - pageOffset.coerceIn(0f, 1f)
                    ).also { scale ->
                        scaleX = scale
                        scaleY = scale
                    }

                    // We animate the alpha, between 50% and 100%
                    alpha = lerp(
                        start = 0.5f,
                        stop = 1f,
                        fraction = 1f - pageOffset.coerceIn(0f, 1f)
                    )
                }
        ) {
            // Card content
        }
    }
}*/

@Composable
fun TitleUI(nowPlayingViewModel: NowPlayingViewModel) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 14.dp), verticalAlignment = Alignment.Bottom, horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Column {
            CustomTextView(
                textData = nowPlayingViewModel.nowPlayingSongArray[nowPlayingViewModel.pagePosition].songTitle,
                fontSize = 20,
                textColor = colorResource(id = R.color.white),
                fontFamily = FontFamily(Font(R.font.lexend_semi_bold)),
                modifier = Modifier,
                textAlignment = TextAlign.Start
            )
            CustomTextView(
                textData = nowPlayingViewModel.nowPlayingSongArray[nowPlayingViewModel.pagePosition].songSubTitle,
                fontSize = 16,
                textColor = colorResource(id = R.color.white),
                fontFamily = FontFamily(Font(R.font.lexend_light)),
                modifier = Modifier.padding(top = 7.dp),
                textAlignment = TextAlign.Start
            )
        }
        Row(modifier = Modifier.offset(y = 15.dp)) {
            IconButton(onClick = { /*TODO*/ }) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_fav),
                    contentDescription = "fav_image",
                    modifier = Modifier.size(20.dp),
                    tint = colorResource(
                        id = if (nowPlayingViewModel.nowPlayingSongArray[nowPlayingViewModel.pagePosition].isFav) R.color.white else R.color.white
                    )
                )
            }
            IconButton(onClick = { /*TODO*/ }) {
                Image(
                    painter = painterResource(id = R.drawable.ic_share),
                    contentDescription = "share_image",
                    modifier = Modifier.size(20.dp),
                )
            }
            IconButton(onClick = { /*TODO*/ }) {
                Image(
                    painter = painterResource(id = R.drawable.ic_more),
                    contentDescription = "more_image",
                    modifier = Modifier.size(20.dp),
                )
            }
        }
    }
}

@Composable
fun ScoreGenerator() {
    var newScore by remember {
        mutableStateOf(0f)
    }
    Column(modifier = Modifier) {
        ArcProgressbar(
            modifier = Modifier.fillMaxWidth(),
            newScore = newScore,
            level = ""
        )
    }
}

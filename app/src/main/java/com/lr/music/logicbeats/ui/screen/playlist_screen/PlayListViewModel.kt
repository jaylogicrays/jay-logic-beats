package com.lr.music.logicbeats.ui.screen.playlist_screen

import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.lifecycle.ViewModel
import com.lr.music.logicbeats.R
import com.lr.music.logicbeats.model.playlist_model.PlaylistModel
import com.lr.music.logicbeats.model.playlist_model.RecentPlayedModel
import com.lr.music.logicbeats.model.your_playlist.PlayListModel

class PlayListViewModel : ViewModel() {
    var userImage = "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg"
    var playListArray = addPlayListArray()
    var playListDataArray : SnapshotStateList<PlayListModel> = addPlayListDataArray()
}

fun addPlayListArray() : ArrayList<PlaylistModel> {
    return arrayListOf(
        PlaylistModel("Playlist", R.drawable.ic_music_libs),
        PlaylistModel("Add Playlist", R.drawable.ic_add)
    )
}

fun addPlayListDataArray(): SnapshotStateList<PlayListModel> {
    return mutableStateListOf(
        PlayListModel("https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg","Top 50 KK Songs"),
        PlayListModel("https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg","Hip Hop Party")
    )
}
package com.lr.music.logicbeats.model.home_screen

data class MadeForYouModel(
    val albumImage : String,
    val albumTitle: String,
    val albumDescription: String,
)

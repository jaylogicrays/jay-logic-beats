package com.lr.music.logicbeats.model.notification

data class NotificationTitleDataModel(
    val index : Int,
    val songImage: String,
    val songDuration: String,
    val songName: String,
    val singerName: String,
    val category : String
)
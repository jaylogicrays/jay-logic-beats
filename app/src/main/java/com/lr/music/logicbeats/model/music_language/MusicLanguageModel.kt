package com.lr.music.logicbeats.model.music_language

data class MusicLanguageModel(var name : String, var image : String, var nameEng : String, var isSelected : Boolean)

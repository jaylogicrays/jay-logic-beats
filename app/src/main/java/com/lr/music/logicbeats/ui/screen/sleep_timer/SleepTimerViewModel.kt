package com.lr.music.logicbeats.ui.screen.sleep_timer

import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel

class SleepTimerViewModel : ViewModel() {
    var minutesTime by  mutableStateOf(0)
//    var time by getTimeInFormat(minutesTime.toLong())
    var timerDataArray  = arrayListOf("Off","15","30","45","60","120")
    var selectedIndex by mutableStateOf(0)
}
package com.lr.music.logicbeats.ui.screen.your_library

import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.lifecycle.ViewModel
import com.lr.music.logicbeats.R
import com.lr.music.logicbeats.model.home_screen.LikedSongModel
import com.lr.music.logicbeats.model.home_screen.TrendingAlbumModel
import com.lr.music.logicbeats.model.playlist_model.PlaylistModel
import com.lr.music.logicbeats.model.playlist_model.RecentPlayedModel
import com.lr.music.logicbeats.ui.screen.liked_screen.addLikedSongArray

class YourLibraryViewModel : ViewModel() {
    var userImage = "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg"
    var playListArray = addPlayListArray()
    var recentPlayedArray: SnapshotStateList<RecentPlayedModel> = addRecentPlayedSongArray()

}

fun addPlayListArray() : ArrayList<PlaylistModel> {
    return arrayListOf(
        PlaylistModel("Add Playlist", R.drawable.ic_add),
        PlaylistModel("Playlist", R.drawable.ic_music_libs),
        PlaylistModel("Artist", R.drawable.ic_mic),
        PlaylistModel("Albums", R.drawable.ic_album),
        )
}

fun addRecentPlayedSongArray(): SnapshotStateList<RecentPlayedModel> {
    return mutableStateListOf(
        RecentPlayedModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem Ispum is Simply",
            "Kumar Sanu, Alka Yagni"
        ),RecentPlayedModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem Ispum is Simply",
            "Kumar Sanu, Alka Yagni"
        ),RecentPlayedModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem Ispum is Simply",
            "Kumar Sanu, Alka Yagni"
        ),RecentPlayedModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem Ispum is Simply",
            "Kumar Sanu, Alka Yagni"
        ),RecentPlayedModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem Ispum is Simply",
            "Kumar Sanu, Alka Yagni"
        ),RecentPlayedModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem Ispum is Simply",
            "Kumar Sanu, Alka Yagni"
        ),RecentPlayedModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem Ispum is Simply",
            "Kumar Sanu, Alka Yagni"
        ),RecentPlayedModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem Ispum is Simply",
            "Kumar Sanu, Alka Yagni"
        ),

    )
}
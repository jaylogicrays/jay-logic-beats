package com.lr.music.logicbeats.ui.screen.playlist_screen

import android.content.Context
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import coil.compose.rememberAsyncImagePainter
import com.lr.music.logicbeats.R
import com.lr.music.logicbeats.common.CustomTextView
import com.lr.music.logicbeats.common.TopTitleWithCenterText
import com.lr.music.logicbeats.common.VerticalSpacer
import com.lr.music.logicbeats.model.playlist_model.PlaylistModel
import com.lr.music.logicbeats.model.your_playlist.PlayListModel

@Composable
fun PlayListScreen(navController: NavHostController, playListViewModel: PlayListViewModel = viewModel()) {
    var context = LocalContext.current
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Black)
            .padding(horizontal = 14.dp)
    ) {
        TitleBarUI(navController = navController, context = context, playListViewModel = playListViewModel)
        VerticalSpacer(29, Color.Transparent)
        PlayListListUI(playListViewModel)
        YourPlayListUI(context, playListViewModel.playListDataArray)
    }
}

@Composable
fun TitleBarUI(navController: NavController, context: Context, playListViewModel: PlayListViewModel) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 10.dp), verticalAlignment = Alignment.CenterVertically
    ) {
        TopTitleWithCenterText(
            centerTitle = context.getString(R.string.your_playlist_text),
            showMenuButton = true,
            userImage = playListViewModel.userImage
        ) {
            navController.navigateUp()
        }
    }
}

@Composable
fun PlayListListUI(playListViewModel: PlayListViewModel) {
    Row {
        playListViewModel.playListArray.forEach { playListItem ->
            PlaylistItem(playListItem = playListItem) {

            }
        }
    }
}

@Composable
fun PlaylistItem(playListItem: PlaylistModel, onClick: () -> Unit) {
    Row(
        modifier = Modifier
            .defaultMinSize()
            .padding(horizontal = 5.dp, vertical = 15.dp)
    ) {
        Box(
            modifier = Modifier
                .width((LocalConfiguration.current.screenWidthDp.dp - 20.dp) / 4)
                .height(122.dp)
                .clip(RoundedCornerShape(53.dp))
                .background(
                    color = colorResource(
                        id = R.color.playlist_bg_color
                    )
                )
                .clickable { onClick() },
        ) {
            Column(modifier = Modifier.fillMaxSize(), horizontalAlignment = Alignment.CenterHorizontally) {
                Box(
                    modifier = Modifier
                        .size(58.dp)
                        .clip(CircleShape)
                        .background(Color.White), contentAlignment = Alignment.Center
                ) {
                    Image(painter = painterResource(id = playListItem.image), contentDescription = "IconImage", modifier = Modifier.size(24.dp))
                }
                Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
                    CustomTextView(
                        textData = playListItem.title,
                        fontSize = 12,
                        textColor = colorResource(
                            id = R.color.white
                        ),
                        fontFamily = FontFamily(Font(R.font.lexend_medium)),
                        modifier = Modifier,
                        textAlignment = TextAlign.Start
                    )
                }
            }

        }
    }
}

@Composable
fun YourPlayListUI(context: Context, playListDataArray: SnapshotStateList<PlayListModel>) {
    Column {
        CustomTextView(
            textData = context.getString(R.string.your_playlist_text),
            fontSize = 16,
            textColor = colorResource(
                id = R.color.white
            ),
            fontFamily = FontFamily(Font(R.font.lexend_medium)),
            modifier = Modifier,
            textAlignment = TextAlign.Start
        )
        VerticalSpacer(28, Color.Transparent)
        playListDataArray.forEachIndexed { index, playListItem ->
            playListItem(playListItem, showDivider = index==(playListDataArray.size-1))
        }
    }
}

@Composable
fun playListItem(playListItem: PlayListModel, showDivider : Boolean = true) {
    Column(modifier = Modifier) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 18.dp), verticalAlignment = Alignment.CenterVertically
        ) {
            Image(
                painter = rememberAsyncImagePainter(playListItem.image),
                contentDescription = "Image",
                modifier = Modifier
                    .size(50.dp)
                    .clip(RoundedCornerShape(10.dp)), contentScale = ContentScale.Crop
            )

            CustomTextView(
                textData = playListItem.playListTitle,
                fontSize = 16,
                textColor = colorResource(id = R.color.white),
                fontFamily = FontFamily(Font(R.font.lexend_medium)),
                modifier = Modifier.padding(start = 17.dp),
                textAlignment = TextAlign.Start
            )
        }
        if (showDivider)
            Spacer(
                modifier = Modifier
                    .padding(top = 18.dp)
                    .height(1.dp)
                    .fillMaxWidth()
                    .background(colorResource(id = R.color.divider_color))
            )
    }
}
package com.lr.music.logicbeats.ui.screen.search_screen

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.lifecycle.ViewModel
import com.lr.music.logicbeats.model.search_screen.SearchScreenModel

class SearchScreenViewModel : ViewModel() {
    var textField by mutableStateOf("")
    var historyDataArray: SnapshotStateList<SearchScreenModel> = addHistoryDataSongArray()
    var tempDataArray: SnapshotStateList<SearchScreenModel> = addHistoryDataSongArray()
}

fun addHistoryDataSongArray(): SnapshotStateList<SearchScreenModel> {
    return mutableStateListOf(
        SearchScreenModel(
            0,
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem1 Ispum is Simply",
            "Kumar Sanu, Alka Yagni"
        ),
        SearchScreenModel(
            1,
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem2 Ispum is Simply",
            "Kumar Sanu, Alka Yagni"
        ),
        SearchScreenModel(
            2,
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem3 Ispum is Simply",
            "Kumar Sanu, Alka Yagni"
        ),
        SearchScreenModel(
            3,
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem4 Ispum is Simply",
            "Kumar Sanu, Alka Yagni"
        ),
        SearchScreenModel(
            4,
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem5 Ispum is Simply",
            "Kumar Sanu, Alka Yagni"
        ),
        SearchScreenModel(
            5,
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem6 Ispum is Simply",
            "Kumar Sanu, Alka Yagni"
        ),
        SearchScreenModel(
            6,
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem7 Ispum is Simply",
            "Kumar Sanu, Alka Yagni"
        ),
        SearchScreenModel(
            7,
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem8 Ispum is Simply",
            "Kumar Sanu, Alka Yagni"
        ),
    )
}

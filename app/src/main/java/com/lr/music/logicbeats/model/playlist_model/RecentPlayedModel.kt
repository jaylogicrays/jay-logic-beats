package com.lr.music.logicbeats.model.playlist_model

data class RecentPlayedModel(
    val songImage: String,
    val songDuration: String,
    val songName: String,
    var singerName: String
)

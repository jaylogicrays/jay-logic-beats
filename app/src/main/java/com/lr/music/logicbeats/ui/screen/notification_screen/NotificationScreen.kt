package com.lr.music.logicbeats.ui.screen.notification_screen

import android.content.Context
import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import coil.compose.rememberAsyncImagePainter
import com.lr.music.logicbeats.R
import com.lr.music.logicbeats.common.CustomTextView
import com.lr.music.logicbeats.common.TopTitleWithCenterText
import com.lr.music.logicbeats.model.notification.NotificationCategoryModel
import com.lr.music.logicbeats.model.notification.NotificationTitleDataModel

@Composable
fun NotificationScreen(navController: NavHostController, notificationScreenViewModel: NotificationScreenViewModel = viewModel()) {
    val context = LocalContext.current
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(colorResource(id = R.color.black))
    ) {
        TitleBarUI(navController = navController, context = context)
        NotificationTitleUI(notificationScreenViewModel, context)
    }
}

@Composable
fun TitleBarUI(navController: NavController, context: Context) {
    Row(modifier = Modifier.fillMaxWidth()) {
        TopTitleWithCenterText(centerTitle = context.getString(R.string.notification_text)) {
            navController.navigateUp()
        }
    }
}

@Composable
fun NotificationTitleUI(notificationScreenViewModel: NotificationScreenViewModel, context: Context) {

    NotificationTitleRowUI(notificationScreenViewModel)

    Column(
        modifier = Modifier
            .fillMaxWidth().padding(horizontal = 14.dp)
            .verticalScroll(rememberScrollState())
    ) {
        CustomTextView(
            textData = context.getString(R.string.what_new_text),
            fontSize = 14,
            textColor = colorResource(id = R.color.white),
            fontFamily = FontFamily(Font(R.font.lexend_bold)),
            modifier = Modifier.padding(top = 30.dp),
            textAlignment = TextAlign.Start
        )

        if (notificationScreenViewModel.selectedSongIndex == -1) {
            notificationScreenViewModel.notificationSongArray.forEachIndexed { index, notificationTitleDataModel ->
                SelectedSongItem(notificationScreenViewModel.notificationSongArray[index])
            }
        } else {
            var filteredArray = mutableListOf<NotificationTitleDataModel>()
            notificationScreenViewModel.notificationSongArray.forEach {
                Log.d("TAG", "NotificationTitleUI:Category ${it.category} :: ${notificationScreenViewModel.notificationSongArray[notificationScreenViewModel.selectedSongIndex].category}")
                if (it.category.equals(notificationScreenViewModel.notificationTitleArray[notificationScreenViewModel.selectedSongIndex].notificationCategory)) {
                    filteredArray.add(it)
                }
            }
            filteredArray.forEach{
                SelectedSongItem(it)
            }
        }
/*
        notificationScreenViewModel.notificationDataArray.forEachIndexed { index, songItem ->
            SelectedSongItem(songItem)
        }*/
    }
}

@Composable
fun NotificationTitleRowUI(notificationScreenViewModel: NotificationScreenViewModel) {
    Row(
        modifier = Modifier
            .fillMaxWidth().padding(start = 14.dp, top = 27.dp)
            .horizontalScroll(rememberScrollState())
    ) {
        if (notificationScreenViewModel.selectedSongIndex == -1)
            notificationScreenViewModel.notificationTitleArray.forEachIndexed { index, notificationCategoryItem ->
                NotificationTopUI(notificationCategoryItem) {
                    if (notificationScreenViewModel.selectedSongIndex != -1)
                        notificationScreenViewModel.notificationTitleArray[notificationScreenViewModel.selectedSongIndex] =
                            notificationScreenViewModel.notificationTitleArray[notificationScreenViewModel.selectedSongIndex].copy(isSelected = false)
                    notificationScreenViewModel.selectedSongIndex = index
                    notificationScreenViewModel.notificationTitleArray[index] =
                        notificationScreenViewModel.notificationTitleArray[index].copy(isSelected = true)
                }
            }
        else {
            Box(
                modifier = Modifier
                    .padding(end = 12.dp)
                    .size(30.dp)
                    .background(Color.Transparent)
                    .clickable {
                        notificationScreenViewModel.notificationTitleArray[notificationScreenViewModel.selectedSongIndex] =
                            notificationScreenViewModel.notificationTitleArray[notificationScreenViewModel.selectedSongIndex].copy(isSelected = false)
                        notificationScreenViewModel.selectedSongIndex = -1
                    }, contentAlignment = Alignment.Center
            ) {
                Image(
                    painter = painterResource(id = R.drawable.ic_close),
                    contentDescription = "close_image",
                    modifier = Modifier.size(20.dp)
                )
            }

            NotificationTopUI(
                notificationScreenViewModel.notificationTitleArray[notificationScreenViewModel.selectedSongIndex],
                isClickable = false, onClick = {})
        }
    }
}

@Composable
fun NotificationTopUI(notificationCategoryItem: NotificationCategoryModel, isClickable: Boolean = true,onClick: () -> Unit) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .padding(end = 10.dp)
            .clip(RoundedCornerShape(48.dp))
            .border(width = 1.dp, color = colorResource(id = R.color.divider_color), shape = RoundedCornerShape(36.dp))
            .background(color = if (notificationCategoryItem.isSelected) colorResource(id = R.color.white) else colorResource(id = R.color.container_color))
            .clickable(onClick = {
                if (isClickable)
                    onClick()
            })
            .padding(vertical = 8.dp, horizontal = 20.dp), contentAlignment = Alignment.Center
    ) {
        CustomTextView(
            textData = notificationCategoryItem.notificationCategory,
            fontSize = 14,
            textColor = colorResource(id = if (notificationCategoryItem.isSelected) R.color.container_color else R.color.white),
            fontFamily = FontFamily(Font(R.font.lexend_light)),
            modifier = Modifier,
            textAlignment = TextAlign.Start
        )
    }
}

@Composable
fun SelectedSongItem(selectedSongItem: NotificationTitleDataModel) {
    Column(modifier = Modifier) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 18.dp), horizontalArrangement = Arrangement.SpaceBetween, verticalAlignment = Alignment.CenterVertically
        ) {
            Row(
                modifier = Modifier, verticalAlignment = Alignment.CenterVertically
            ) {
                Image(
                    painter = rememberAsyncImagePainter(selectedSongItem.songImage),
                    contentDescription = "Person Image",
                    modifier = Modifier
                        .size(50.dp)
                        .clip(RoundedCornerShape(10.dp)), contentScale = ContentScale.Crop
                )
                Column(modifier = Modifier.padding(start = 17.dp)) {
                    CustomTextView(
                        textData = selectedSongItem.songName,
                        fontSize = 14,
                        textColor = colorResource(id = R.color.white),
                        fontFamily = FontFamily(Font(R.font.lexend_medium)),
                        modifier = Modifier,
                        textAlignment = TextAlign.Start
                    )
                    Row(modifier = Modifier.padding(top = 7.dp), verticalAlignment = Alignment.CenterVertically) {
                        CustomTextView(
                            textData = selectedSongItem.songDuration,
                            fontSize = 10,
                            textColor = colorResource(id = R.color.gray_color),
                            fontFamily = FontFamily(Font(R.font.lexend_regular)),
                            modifier = Modifier,
                            textAlignment = TextAlign.Start,
                        )
                        Box(
                            modifier = Modifier
                                .padding(horizontal = 5.dp)
                                .size(3.dp)
                                .clip(CircleShape)
                                .background(colorResource(id = R.color.gray_color))
                        )
                        CustomTextView(
                            textData = selectedSongItem.singerName,
                            fontSize = 10,
                            textColor = colorResource(id = R.color.gray_color),
                            fontFamily = FontFamily(Font(R.font.lexend_regular)),
                            modifier = Modifier,
                            textAlignment = TextAlign.Start,
                        )
                    }
                }
            }
        }
        Spacer(
            modifier = Modifier
                .padding(top = 18.dp)
                .height(1.dp)
                .fillMaxWidth()
                .background(colorResource(id = R.color.divider_color))
        )
    }
}

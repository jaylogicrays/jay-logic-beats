package com.lr.music.logicbeats.model.home_screen

data class DiscoverMusicModel(val discoverTitle : String,val discoverTitleDataArray : ArrayList<DiscoverTitleDataModel>, var isSelected : Boolean)

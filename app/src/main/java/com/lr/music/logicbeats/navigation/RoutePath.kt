package com.lr.music.logicbeats.navigation

enum class RoutePath {
    SplashScreen,
    ExploreMoreScreen,
    LoginScreen,
    HomeMainScreen,
    SearchScreen,
    NotificationScreen,
    SettingScreen,
    EditProfileScreen,
    MusicLanguageScreen,
    ArtistSelection,
    SleepTimer,
    NowPlayingScreen,
    ArtistSongScreen,
    SongDetailsScreen,
    CreateNewPlaylistScreen,
    PlayListScreen,
    PlayListDetailsScreen,
    LikedSongScreen
}
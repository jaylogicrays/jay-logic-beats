package com.lr.music.logicbeats.ui.screen.playlist_details

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.lifecycle.ViewModel
import com.lr.music.logicbeats.model.artist_song.ArtistSongModel
import com.lr.music.logicbeats.model.playlist_model.RecentPlayedModel
import com.lr.music.logicbeats.ui.screen.artist_song.getArtistData

class PlayListDetailsViewModel : ViewModel() {
    val albumImage = "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg"
    val singerImage = "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg"
    val title = "KK Album"
    val subTitle = "Top 50 Kk Songs"
    val singerName = "Ami Patel"
    val releaseDate = "19th 2020"
    val duration = "38 min 38 s"
    var isShuffleSelected by mutableStateOf(false)
    var isRepeatSelected by mutableStateOf(false)
    var artistSongArray: SnapshotStateList<RecentPlayedModel> = addPlayListDataArray()

}

fun addPlayListDataArray(): SnapshotStateList<RecentPlayedModel> {
    return mutableStateListOf(
        RecentPlayedModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem Ispum is Simply",
            "Kumar Sanu, Alka Yagni"
        ),RecentPlayedModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem Ispum is Simply",
            "Kumar Sanu, Alka Yagni"
        ),RecentPlayedModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem Ispum is Simply",
            "Kumar Sanu, Alka Yagni"
        ),RecentPlayedModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem Ispum is Simply",
            "Kumar Sanu, Alka Yagni"
        ),RecentPlayedModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem Ispum is Simply",
            "Kumar Sanu, Alka Yagni"
        ),RecentPlayedModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem Ispum is Simply",
            "Kumar Sanu, Alka Yagni"
        ),RecentPlayedModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem Ispum is Simply",
            "Kumar Sanu, Alka Yagni"
        ),RecentPlayedModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem Ispum is Simply",
            "Kumar Sanu, Alka Yagni"
        ),

        )
}

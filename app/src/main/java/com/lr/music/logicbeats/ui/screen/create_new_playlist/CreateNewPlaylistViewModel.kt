package com.lr.music.logicbeats.ui.screen.create_new_playlist

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel

class CreateNewPlaylistViewModel() : ViewModel() {
    var textField by mutableStateOf("")
}
package com.lr.music.logicbeats.model.home_screen

data class TrendingArtistModel(
    val profileImage : String,
    val name: String,
)

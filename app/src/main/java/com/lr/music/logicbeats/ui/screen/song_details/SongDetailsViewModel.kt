package com.lr.music.logicbeats.ui.screen.song_details

import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.lifecycle.ViewModel
import com.lr.music.logicbeats.model.artist_song.SingerModel
import com.lr.music.logicbeats.model.music_language.MusicLanguageModel
import com.lr.music.logicbeats.model.notification.NotificationTitleDataModel
import com.lr.music.logicbeats.ui.screen.music_language.addMusicLanguageArray

class SongDetailsViewModel : ViewModel() {
    val singerImage = "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg"
    val songName = "Kheech Meri Photo"
    val movieName = "Sanam Teri Kasam"
    val releaseDate = "5th February 2016"
    var singerDetailsArray: SnapshotStateList<SingerModel> = addSingerDetailsArray()
    var movieCastArray: SnapshotStateList<SingerModel> = addMovieCastArray()

}

fun addSingerDetailsArray(): SnapshotStateList<SingerModel> {
    return mutableStateListOf(
        SingerModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Darshan Raval",
        ),
        SingerModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Neeti Mohan",
        ),
        SingerModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Akasa",
        ),
        SingerModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Darshan Raval",
        ),
    )
}

fun addMovieCastArray(): SnapshotStateList<SingerModel> {
    return mutableStateListOf(
        SingerModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Darshan Raval",
        ),
        SingerModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Neeti Mohan",
        ),
        SingerModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Akasa",
        ),
        SingerModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Darshan Raval",
        ),
    )
}
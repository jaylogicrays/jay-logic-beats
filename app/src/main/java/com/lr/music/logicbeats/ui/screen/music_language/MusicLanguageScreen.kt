package com.lr.music.logicbeats.ui.screen.music_language

import android.content.Context
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import coil.compose.rememberAsyncImagePainter
import com.lr.music.logicbeats.R
import com.lr.music.logicbeats.common.CustomTextView
import com.lr.music.logicbeats.common.TopTitleWithCenterText
import com.lr.music.logicbeats.common.VerticalSpacer
import com.lr.music.logicbeats.model.music_language.MusicLanguageModel

@Composable
fun MusicLanguageScreen(navController: NavHostController, musicLanguageViewModel: MusicLanguageViewModel = viewModel()) {
    var context = LocalContext.current
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(colorResource(id = R.color.black))
                .padding(horizontal = 14.dp)
        ) {
            TitleBarUI(navController = navController, context = context)
            DisplayLanguageLUI(musicLanguageViewModel)
        }
}

@Composable
fun TitleBarUI(navController: NavController, context: Context) {
    Row(modifier = Modifier.fillMaxWidth()) {
        TopTitleWithCenterText(centerTitle = context.getString(R.string.music_language_text)) {
            navController.navigateUp()
        }
    }
}

@Composable
fun DisplayLanguageLUI(musicLanguageViewModel: MusicLanguageViewModel) {
    VerticalSpacer(29, Color.Transparent)
    LazyVerticalGrid(
        columns = GridCells.Fixed(2),
        horizontalArrangement = Arrangement.spacedBy(15.dp),
        verticalArrangement = Arrangement.spacedBy(18.dp)
    ) {
        items(musicLanguageViewModel.musicLanguageArray.size) { index ->
            MusicItem(musicLanguageViewModel.musicLanguageArray[index], musicLanguageViewModel, index, onClick = {
                musicLanguageViewModel.musicLanguageArray[index] =
                    musicLanguageViewModel.musicLanguageArray[index].copy(isSelected = !musicLanguageViewModel.musicLanguageArray[index].isSelected)

            })
        }
    }
}

@Composable
fun MusicItem(musicLanguageModel: MusicLanguageModel, musicLanguageViewModel: MusicLanguageViewModel, index: Int, onClick: (() -> Unit)) {
    Box(modifier = Modifier.padding(top = 2.dp), contentAlignment = Alignment.TopEnd) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .height(80.dp)
                .clip(RoundedCornerShape(4.dp))
                .background(
                    if (musicLanguageModel.isSelected) colorResource(id = musicLanguageViewModel.selectedColorArray[index % 4]) else colorResource(
                        id = R.color.dark_jungle_green_color
                    )
                )
                .clickable { onClick() }, contentAlignment = Alignment.TopEnd
        ) {
            if (index % 2 == 0)
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Image(
                        painter = rememberAsyncImagePainter(
                            model = musicLanguageViewModel.singerImage
                        ), contentDescription = "singer_image", modifier = Modifier
                            .fillMaxHeight()
                            .widthIn(0.dp, 80.dp), contentScale = ContentScale.Crop
                    )
                    Column(
                        modifier = Modifier
                            .fillMaxHeight()
                            .padding(end = 14.dp), verticalArrangement = Arrangement.Center
                    ) {
                        CustomTextView(
                            textData = musicLanguageModel.name,
                            fontSize = 20,
                            textColor = colorResource(id = R.color.white),
                            fontFamily = FontFamily(Font(R.font.lexend_bold)),
                            modifier = Modifier,
                            textAlignment = TextAlign.Start
                        )
                        if (!musicLanguageModel.nameEng.equals("English"))
                            CustomTextView(
                                textData = musicLanguageModel.nameEng,
                                fontSize = 12,
                                textColor = colorResource(id = R.color.white),
                                fontFamily = FontFamily(Font(R.font.lexend_bold)),
                                modifier = Modifier.padding(top = 7.dp),
                                textAlignment = TextAlign.Start
                            )
                    }
                }
            else
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Column(
                        modifier = Modifier
                            .fillMaxHeight()
                            .padding(start = 14.dp), verticalArrangement = Arrangement.Center
                    ) {
                        CustomTextView(
                            textData = musicLanguageModel.name,
                            fontSize = 20,
                            textColor = colorResource(id = R.color.white),
                            fontFamily = FontFamily(Font(R.font.lexend_bold)),
                            modifier = Modifier,
                            textAlignment = TextAlign.Start
                        )
                        if (!musicLanguageModel.nameEng.equals("English"))
                            CustomTextView(
                                textData = musicLanguageModel.nameEng,
                                fontSize = 12,
                                textColor = colorResource(id = R.color.white),
                                fontFamily = FontFamily(Font(R.font.lexend_bold)),
                                modifier = Modifier.padding(top = 7.dp),
                                textAlignment = TextAlign.Start
                            )
                    }
                    Image(
                        painter = rememberAsyncImagePainter(
                            model = musicLanguageViewModel.singerImage
                        ), contentDescription = "singer_image", modifier = Modifier
                            .fillMaxHeight()
                            .widthIn(0.dp, 80.dp), contentScale = ContentScale.Crop
                    )
                }
        }

        if (musicLanguageModel.isSelected) {
            Box(
                modifier = Modifier
                    .offset(x = 2.dp, y = -2.dp)
                    .size(20.dp)
                    .clip(CircleShape)
                    .background(Color.White), contentAlignment = Alignment.Center
            ) {
                Image(
                    painter = painterResource(id = R.drawable.ic_artist_done),
                    contentDescription = "done_image",
                    modifier = Modifier
                        .width(9.dp)
                        .height(6.dp)
                )
            }
        }
    }
}

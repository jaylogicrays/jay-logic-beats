package com.lr.music.logicbeats.ui.screen.song_details

import android.content.Context
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import coil.compose.rememberAsyncImagePainter
import com.lr.music.logicbeats.R
import com.lr.music.logicbeats.common.CustomTextView
import com.lr.music.logicbeats.common.VerticalSpacer
import com.lr.music.logicbeats.model.artist_song.SingerModel

@Composable
fun SongDetailsScreen(navController: NavHostController, songDetailsViewModel: SongDetailsViewModel = viewModel()) {
    val context = LocalContext.current
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(colorResource(id = R.color.black))
    ) {
        ImageUI(songDetailsViewModel, navController, context)
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 14.dp)
                .verticalScroll(rememberScrollState())
        ) {
            SingerDetailsUI(songDetailsViewModel, context)
        }
    }
}

@Composable
fun ImageUI(songDetailsViewModel: SongDetailsViewModel, navController: NavHostController, context: Context) {

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height(410.dp), contentAlignment = Alignment.BottomEnd
    ) {
        Image(
            painter = rememberAsyncImagePainter(model = songDetailsViewModel.singerImage),
            contentDescription = "song_image",
            modifier = Modifier
                .fillMaxSize()
                .padding(bottom = 25.dp), contentScale = ContentScale.Crop
        )
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 14.dp)
                .clip(RoundedCornerShape(8.dp))
                .border(
                    brush = Brush.horizontalGradient(
                        listOf(
                            colorResource(id = R.color.boulder_color),
                            colorResource(id = R.color.davy_grey_color)
                        )
                    ),
                    width = 1.dp,
                    shape = RoundedCornerShape(8.dp)
                )
                .background(colorResource(id = R.color.bg_color))
                .padding(horizontal = 10.dp, vertical = 14.dp), contentAlignment = Alignment.Center
        ) {
            Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
                Box(modifier = Modifier.clip(RoundedCornerShape(8.dp)), contentAlignment = Alignment.Center) {
                    Image(
                        painter = rememberAsyncImagePainter(model = songDetailsViewModel.singerImage),
                        contentDescription = "image",
                        modifier = Modifier.size(110.dp), contentScale = ContentScale.Crop
                    )
                    Image(
                        painter = painterResource(id = R.drawable.ic_play_button),
                        contentDescription = "play_button",
                        modifier = Modifier.size(50.dp)
                    )
                }
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 14.dp)
                ) {
                    CustomTextView(
                        textData = songDetailsViewModel.songName,
                        fontSize = 20,
                        textColor = colorResource(id = R.color.white),
                        fontFamily = FontFamily(Font(R.font.lexend_medium)),
                        modifier = Modifier.padding(bottom = 10.dp),
                        textAlignment = TextAlign.Start
                    )
                    VerticalSpacer(1, colorResource(id = R.color.gray_color))
                    CustomTextView(
                        textData = "Movie Name : ${songDetailsViewModel.movieName}",
                        fontSize = 12,
                        textColor = colorResource(id = R.color.gray_color),
                        fontFamily = FontFamily(Font(R.font.lexend_regular)),
                        modifier = Modifier.padding(top = 10.dp),
                        textAlignment = TextAlign.Start
                    )
                    CustomTextView(
                        textData = "Release Date : ${songDetailsViewModel.releaseDate}",
                        fontSize = 12,
                        textColor = colorResource(id = R.color.gray_color),
                        fontFamily = FontFamily(Font(R.font.lexend_regular)),
                        modifier = Modifier.padding(top = 10.dp),
                        textAlignment = TextAlign.Start
                    )
                }
            }
        }
        /* Column(modifier = Modifier.fillMaxSize(), verticalArrangement = Arrangement.SpaceBetween) {
             TitleBarUI(navController = navController, context = context)
             Column(
                 modifier = Modifier
                     .fillMaxWidth()
                     .padding(start = 14.dp), verticalArrangement = Arrangement.SpaceBetween
             ) {
                 CustomTextView(
                     textData = artistSongScreenViewModel.artistAlbumName,
                     fontSize = 12,
                     textColor = colorResource(id = R.color.silver_chalice_color),
                     fontFamily = FontFamily(Font(R.font.lexend_semi_bold)),
                     modifier = Modifier,
                     textAlignment = TextAlign.Start
                 )
                 CustomTextView(
                     textData = artistSongScreenViewModel.artistTitleName,
                     fontSize = 18,
                     textColor = colorResource(id = R.color.white),
                     fontFamily = FontFamily(Font(R.font.lexend_semi_bold)),
                     modifier = Modifier.padding(top = 10.dp),
                     textAlignment = TextAlign.Start
                 )
                 Box(modifier = Modifier.fillMaxWidth(), contentAlignment = Alignment.BottomEnd) {
                     Image(
                         painter = painterResource(id = R.drawable.ic_play_button),
                         contentDescription = "play_icon",
                         modifier = Modifier
                             .padding(end = 23.dp)
                             .size(50.dp),
                     )
                 }
             }
         }*/
    }
}

@Composable
fun SingerDetailsUI(songDetailsViewModel: SongDetailsViewModel, context: Context) {
    CustomTextView(
        textData = context.getString(R.string.singer_details),
        fontSize = 18,
        textColor = colorResource(id = R.color.white),
        fontFamily = FontFamily(Font(R.font.lexend_semi_bold)),
        modifier = Modifier.padding(top = 26.dp),
        textAlignment = TextAlign.Start
    )
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 21.dp)
            .horizontalScroll(rememberScrollState())
    ) {
        songDetailsViewModel.singerDetailsArray.forEach { songItem ->
            SongItem(songItem)
            Column(
                modifier = Modifier
                    .clip(RoundedCornerShape(6.dp)).padding(end = 9.dp)
                    .background(colorResource(id = R.color.playlist_bg_color))
                    .padding(10.dp)
                    , horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.Center
            ) {
                Image(
                    painter = rememberAsyncImagePainter(model = songItem.singerImage),
                    contentDescription = "singer_image",
                    modifier = Modifier.size(90.dp).clip(CircleShape), contentScale = ContentScale.Crop
                )
                CustomTextView(
                    textData = songItem.singerName,
                    fontSize = 10,
                    textColor = colorResource(id = R.color.white),
                    fontFamily = FontFamily(Font(R.font.lexend_semi_bold)),
                    modifier = Modifier.padding(top = 10.dp),
                    textAlignment = TextAlign.Start
                )
            }

        }

    }
}


@Composable
fun MovieCastUI(songDetailsViewModel: SongDetailsViewModel, context: Context) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 30.dp), horizontalArrangement = Arrangement.SpaceBetween, verticalAlignment = Alignment.CenterVertically
    ) {
        CustomTextView(
            textData = context.getString(R.string.movie_cast),
            fontSize = 18,
            textColor = colorResource(id = R.color.white),
            fontFamily = FontFamily(Font(R.font.lexend_medium)),
            modifier = Modifier,
            textAlignment = TextAlign.Start
        )
        Row(verticalAlignment = Alignment.CenterVertically) {
            CustomTextView(
                textData = context.getString(R.string.see_all_text),
                fontSize = 10,
                textColor = colorResource(id = R.color.gray_color),
                fontFamily = FontFamily(Font(R.font.lexend_medium)),
                modifier = Modifier,
                textAlignment = TextAlign.Start
            )
            Image(
                painter = painterResource(id = R.drawable.ic_arrow),
                contentDescription = "Image",
                modifier = Modifier
                    .padding(start = 5.dp)
                    .size(16.dp)
            )
        }
    }
}

@Composable
fun SongItem(songItem: SingerModel) {
    Column(
        modifier = Modifier
            .clip(RoundedCornerShape(6.dp)).padding(end = 9.dp)
            .background(colorResource(id = R.color.playlist_bg_color))
            .padding(10.dp)
        , horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.Center
    ) {
        Image(
            painter = rememberAsyncImagePainter(model = songItem.singerImage),
            contentDescription = "singer_image",
            modifier = Modifier.size(90.dp).clip(CircleShape), contentScale = ContentScale.Crop
        )
        CustomTextView(
            textData = songItem.singerName,
            fontSize = 10,
            textColor = colorResource(id = R.color.white),
            fontFamily = FontFamily(Font(R.font.lexend_semi_bold)),
            modifier = Modifier.padding(top = 10.dp),
            textAlignment = TextAlign.Start
        )
    }
}

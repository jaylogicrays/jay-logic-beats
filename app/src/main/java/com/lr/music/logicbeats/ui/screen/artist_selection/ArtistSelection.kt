package com.lr.music.logicbeats.ui.screen.artist_selection

import android.content.Context
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.paint
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import coil.compose.rememberAsyncImagePainter
import com.lr.music.logicbeats.R
import com.lr.music.logicbeats.common.CustomButton
import com.lr.music.logicbeats.common.CustomSearchView
import com.lr.music.logicbeats.common.CustomTextView
import com.lr.music.logicbeats.common.TopTitleWithCenterText
import com.lr.music.logicbeats.common.VerticalSpacer
import com.lr.music.logicbeats.model.artist_selection.ArtistCategoryModel
import com.lr.music.logicbeats.model.artist_selection.ArtistLanguageArray

@Composable
fun ArtistSelection(navController: NavHostController, artistSelectionViewModel: ArtistSelectionViewModel = viewModel()) {
    val context = LocalContext.current
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(colorResource(id = R.color.black))
    ) {
        TitleBarUI(navController = navController, context = context)
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 14.dp)
        ) {
            SearchArtistSelectionUI(artistSelectionViewModel, context)
            VerticalSpacer(20, Color.Transparent)
            LanguageSelectionRowUI(artistSelectionViewModel)
            VerticalSpacer(20, Color.Transparent)
            ArtistUI(artistSelectionViewModel)
        }
    }
}

@Composable
fun TitleBarUI(navController: NavController, context: Context) {
    Row(modifier = Modifier.fillMaxWidth()) {
        TopTitleWithCenterText(centerTitle = context.getString(R.string.artist_selection_text)) {
            navController.navigateUp()
        }
    }
}

@Composable
fun SearchArtistSelectionUI(artistSelectionViewModel: ArtistSelectionViewModel, context: Context) {
    CustomSearchView(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 18.dp)
            .shadow(elevation = 5.dp, shape = RoundedCornerShape(10.dp))
            .clip(RoundedCornerShape(10.dp))
            .height(50.dp),
        hintsString = context.getString(R.string.search_hint_text),
        textField = artistSelectionViewModel.textField.value
    ) {
        artistSelectionViewModel.textField.value = it
    }
}

@Composable
fun LanguageSelectionRowUI(artistSelectionViewModel: ArtistSelectionViewModel) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .horizontalScroll(rememberScrollState())
    ) {
        if (artistSelectionViewModel.artistSongSelectedIndex == -1)
            artistSelectionViewModel.artistLanguageArray.forEachIndexed { index, artistCategoryItem ->
                LanguageSelectionUI(artistCategoryItem) {
                    if (artistSelectionViewModel.artistSongSelectedIndex != -1)
                        artistSelectionViewModel.artistLanguageArray[artistSelectionViewModel.artistSongSelectedIndex] =
                            artistSelectionViewModel.artistLanguageArray[artistSelectionViewModel.artistSongSelectedIndex].copy(isSelected = false)
                    artistSelectionViewModel.artistSongSelectedIndex = index
                    artistSelectionViewModel.artistLanguageArray[index] =
                        artistSelectionViewModel.artistLanguageArray[index].copy(isSelected = true)
                }
            }
        else {
            Box(
                modifier = Modifier
                    .padding(end = 12.dp)
                    .size(30.dp)
                    .background(Color.Transparent)
                    .clickable {
                        artistSelectionViewModel.artistLanguageArray[artistSelectionViewModel.artistSongSelectedIndex] =
                            artistSelectionViewModel.artistLanguageArray[artistSelectionViewModel.artistSongSelectedIndex].copy(isSelected = false)
                        artistSelectionViewModel.artistSongSelectedIndex = -1
                    }, contentAlignment = Alignment.Center
            ) {
                Image(
                    painter = painterResource(id = R.drawable.ic_close),
                    contentDescription = "close_image",
                    modifier = Modifier.size(20.dp)
                )
            }

            LanguageSelectionUI(
                artistSelectionViewModel.artistLanguageArray[artistSelectionViewModel.artistSongSelectedIndex],
                isClickable = false, onClick = {})
        }
    }
}

@Composable
fun LanguageSelectionUI(artistSelectedItem: ArtistCategoryModel, isClickable: Boolean = true, onClick: () -> Unit) {
    Box(
        modifier = Modifier
            .padding(end = 10.dp)
            .clip(RoundedCornerShape(48.dp))
            .border(width = 1.dp, color = colorResource(id = R.color.divider_color), shape = RoundedCornerShape(36.dp))
            .background(color = if (artistSelectedItem.isSelected) colorResource(id = R.color.white) else colorResource(id = R.color.container_color))
            .clickable(onClick = {
                if (isClickable)
                    onClick()
            })
            .padding(vertical = 8.dp, horizontal = 20.dp), contentAlignment = Alignment.Center
    ) {
        CustomTextView(
            textData = artistSelectedItem.categoryName,
            fontSize = 14,
            textColor = colorResource(id = if (artistSelectedItem.isSelected) R.color.container_color else R.color.white),
            fontFamily = FontFamily(Font(R.font.lexend_regular)),
            modifier = Modifier,
            textAlignment = TextAlign.Start
        )
    }
}

@Composable
fun ArtistUI(artistSelectionViewModel: ArtistSelectionViewModel) {
    Box(contentAlignment = Alignment.BottomCenter) {
        LazyVerticalGrid(
            columns = GridCells.Fixed(3),
            horizontalArrangement = Arrangement.spacedBy(10.dp),
            verticalArrangement = Arrangement.spacedBy(20.dp)
        ) {
            if (artistSelectionViewModel.artistSongSelectedIndex == -1) {
                items(artistSelectionViewModel.artistDataArray.size) { index ->
                    ArtistUIItem(artistSelectionViewModel.artistDataArray[index], onClick = {
                        artistSelectionViewModel.artistDataArray[index] =
                            artistSelectionViewModel.artistDataArray[index].copy(isArtistSelected = !artistSelectionViewModel.artistDataArray[index].isArtistSelected)
                        if (artistSelectionViewModel.artistDataArray[index].isArtistSelected) {
                            artistSelectionViewModel.selectedSongArray.add(artistSelectionViewModel.artistDataArray[index])
                        } else {
                            val itr = artistSelectionViewModel.selectedSongArray.iterator()
                            while (itr.hasNext()) {
                                if (itr.next().index == artistSelectionViewModel.artistDataArray[index].index) {
                                    itr.remove()
                                    break
                                }
                            }
                        }
                    })
                }
            } else {
                var filteredArray = arrayListOf<ArtistLanguageArray>()
                artistSelectionViewModel.artistDataArray.forEach {
                    if (it.language.equals(artistSelectionViewModel.artistLanguageArray[artistSelectionViewModel.artistSongSelectedIndex].categoryName)) {
                        filteredArray.add(it)
                    }
                }
                items(filteredArray.size) { index ->
                    ArtistUIItem(filteredArray[index], onClick = {
                        if (artistSelectionViewModel.artistDataArray[index].isArtistSelected) {
                            artistSelectionViewModel.selectedSongArray.add(artistSelectionViewModel.artistDataArray[index])
                        } else {
                            artistSelectionViewModel.selectedSongArray.remove(artistSelectionViewModel.artistDataArray[index])
                        }
                    })
                }
            }
        }

        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            CustomTextView(
                textData = "${artistSelectionViewModel.selectedSongArray.size} Selected",
                fontSize = 18,
                textColor = colorResource(id = R.color.white),
                fontFamily = FontFamily(Font(R.font.lexend_medium)),
                modifier = Modifier,
                textAlignment = TextAlign.Start
            )
            CustomButton(
                text = LocalContext.current.getString(R.string.done_text),
                modifier = Modifier
                    .padding(top = 10.dp)
                    .height(50.dp)
                    .fillMaxWidth(),
                fontColor = Color.Black,
                fontSize = 16,
                fontFamily = FontFamily(Font(R.font.lexend_bold)),
                backgroundColor = ButtonDefaults.buttonColors(colorResource(id = R.color.white)),
                corner = RoundedCornerShape(34.dp),
                onClick = {
                }
            )
        }
    }
}

@Composable
fun ArtistUIItem(
    artistDataItem: ArtistLanguageArray, onClick: () -> Unit
) {
    Box(
        modifier = Modifier
            .padding(end = 10.dp)
            .clip(shape = RoundedCornerShape(8.dp))
            .size(109.dp)
            .paint(
                rememberAsyncImagePainter(artistDataItem.artistImage),
                contentScale = ContentScale.FillBounds
            )
            .clickable {
                onClick()
//                       artistDataItem.isArtistSelected = !artistDataItem.isArtistSelected
            },
        contentAlignment = Alignment.BottomEnd
    ) {
        Box {
            Column(modifier = Modifier.fillMaxSize(), verticalArrangement = Arrangement.SpaceBetween, horizontalAlignment = Alignment.End) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(Color.Transparent), horizontalArrangement = Arrangement.End
                ) {
                    if (artistDataItem.isArtistSelected)
                        Box(
                            modifier = Modifier
                                .size(20.dp)
                                .clip(CircleShape)
                                .background(Color.White), contentAlignment = Alignment.Center
                        ) {
                            Image(
                                painter = painterResource(id = R.drawable.ic_artist_done),
                                contentDescription = "done_image",
                                modifier = Modifier
                                    .width(9.dp)
                                    .height(6.dp)
                            )
                        }
                }

                Surface(
                    color = colorResource(id = R.color.onyx_color).copy(alpha = 0.6f),
                    modifier = Modifier
                        .width(109.dp)
                        .height(28.dp)
                ) {
                    Column(
                        modifier = Modifier
                            .fillMaxHeight()
                            .padding(start = 14.dp), verticalArrangement = Arrangement.Center
                    ) {
                        CustomTextView(
                            textData = artistDataItem.artistName,
                            fontSize = 14,
                            textColor = colorResource(id = R.color.white),
                            fontFamily = FontFamily(Font(R.font.lexend_medium)),
                            modifier = Modifier,
                            textAlignment = TextAlign.Start
                        )
                    }
                }
            }
            if (!artistDataItem.isArtistSelected) {
                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .background(color = colorResource(id = R.color.black).copy(alpha = 0.6f))
                )
            }
        }
    }
}
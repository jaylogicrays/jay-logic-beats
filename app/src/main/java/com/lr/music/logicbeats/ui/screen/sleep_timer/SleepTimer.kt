package com.lr.music.logicbeats.ui.screen.sleep_timer

import android.content.Context
import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import com.lr.music.logicbeats.R
import com.lr.music.logicbeats.common.CustomTextView
import com.lr.music.logicbeats.common.TopTitleWithCenterText
import com.lr.music.logicbeats.common.VerticalSpacer
import com.lr.music.logicbeats.common.getTimeInFormat

@Composable
fun SleepTimer(navController: NavHostController, sleepTimerViewModel: SleepTimerViewModel = viewModel()) {
    val context = LocalContext.current
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(colorResource(id = R.color.black)), horizontalAlignment = Alignment.CenterHorizontally
    ) {
        TitleBarUI(navController = navController, context = context)
        TimerUI(sleepTimerViewModel)
        VerticalSpacer(17, Color.Transparent)
        IncrementUI(sleepTimerViewModel)
        TimerDataUI(sleepTimerViewModel)
    }
}

@Composable
fun TitleBarUI(navController: NavController, context: Context) {
    Row(modifier = Modifier.fillMaxWidth()) {
        TopTitleWithCenterText(centerTitle = context.getString(R.string.sleep_timer)) {
            navController.navigateUp()
        }
    }
}

@Composable
fun TimerUI(sleepTimerViewModel: SleepTimerViewModel) {
    Row(modifier = Modifier.padding(top = 80.dp)) {
        CustomTextView(
            textData = String.format("%02d:%02d", sleepTimerViewModel.minutesTime / 60, sleepTimerViewModel.minutesTime % 60),
            fontSize = 54,
            textColor = colorResource(id = R.color.white),
            fontFamily = FontFamily(Font(R.font.lexend_medium)),
            modifier = Modifier,
            textAlignment = TextAlign.Start
        )
        Image(
            painter = painterResource(id = R.drawable.ic_timer), contentDescription = "timer_image", modifier = Modifier
                .width(19.dp)
                .height(21.dp)
        )
    }
}

@Composable
fun IncrementUI(sleepTimerViewModel: SleepTimerViewModel) {
    Row {
        IconButton(
            onClick = {
                if (sleepTimerViewModel.minutesTime > 0)
                    sleepTimerViewModel.minutesTime--
                Log.d("TAG", "IncrementUI: Time ${sleepTimerViewModel.minutesTime}")
            },
            modifier = Modifier
                .clip(CircleShape)
                .border(width = 1.dp, color = colorResource(id = R.color.divider_color), shape = CircleShape)
                .background(color = colorResource(id = R.color.playlist_bg_color), shape = CircleShape)
                .size(40.dp),
        ) {
            Image(painter = painterResource(id = R.drawable.ic_minus), contentDescription = "minus_image", modifier = Modifier.width(12.dp))
        }
        IconButton(
            onClick = {
                sleepTimerViewModel.minutesTime = sleepTimerViewModel.minutesTime + 1
                Log.d("TAG", "IncrementUI: Time ${sleepTimerViewModel.minutesTime}")
            },
            modifier = Modifier
                .clip(CircleShape)
                .padding(start = 10.dp)
                .border(width = 1.dp, color = colorResource(id = R.color.divider_color), shape = CircleShape)
                .background(color = colorResource(id = R.color.playlist_bg_color), shape = CircleShape)
                .size(40.dp),
        ) {
            Image(painter = painterResource(id = R.drawable.ic_plus), contentDescription = "minus_image", modifier = Modifier.width(12.dp))
        }
    }

    Image(
        painter = painterResource(id = R.drawable.ic_divider_timer), contentDescription = "divider_image", modifier = Modifier
            .padding(top = 30.dp)
            .width(347.dp)
            .height(2.dp)
    )
}

@Composable
fun TimerDataUI(sleepTimerViewModel: SleepTimerViewModel) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 14.dp)
            .padding(top = 26.dp)
            .background(color = colorResource(id = R.color.playlist_bg_color), shape = RoundedCornerShape(4.dp))
    ) {
        Column {
            sleepTimerViewModel.timerDataArray.forEachIndexed { index, timerDataItem ->
                TimerUIItem(timerDataItem, sleepTimerViewModel.selectedIndex == index, sleepTimerViewModel.timerDataArray.size - 1 == index) {
                    sleepTimerViewModel.selectedIndex = index
                    if (index != 0)
                        sleepTimerViewModel.minutesTime = timerDataItem.toInt()
                    else
                        sleepTimerViewModel.minutesTime = 0
                }
            }
        }
    }
}

@Composable
fun TimerUIItem(timerDataItem: String, isSelected: Boolean, isLast: Boolean, onClick: () -> Unit) {
    Box(modifier = Modifier
        .clickable { onClick() }) {
        Box(contentAlignment = Alignment.BottomEnd) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(40.dp)
                    .padding(horizontal = 14.dp), horizontalArrangement = Arrangement.SpaceBetween, verticalAlignment = Alignment.CenterVertically
            ) {
                CustomTextView(
                    textData = if (timerDataItem.equals("Off")) timerDataItem else getTimeInFormat(timerDataItem.toLong()),
                    fontSize = 14,
                    textColor = colorResource(id = R.color.white),
                    fontFamily = FontFamily(Font(R.font.lexend_regular)),
                    modifier = Modifier,
                    textAlignment = TextAlign.Start
                )

                if (isSelected)
                    Image(
                        painter = painterResource(id = R.drawable.ic_done),
                        contentDescription = "done_image",
                        modifier = Modifier
                            .padding(end = 5.dp)
                            .width(10.dp)
                            .height(8.dp)
                    )
            }
            if (!isLast)
                VerticalSpacer(1, colorResource(id = R.color.divider_color))
        }
    }
}
package com.lr.music.logicbeats.ui.screen

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.lr.music.logicbeats.R
import com.lr.music.logicbeats.common.CustomButton
import com.lr.music.logicbeats.common.CustomTextField
import com.lr.music.logicbeats.common.CustomTextView
import com.lr.music.logicbeats.common.VerticalSpacer
import com.lr.music.logicbeats.navigation.RoutePath

@Composable
fun LoginScreen(navController: NavHostController) {
    var context = LocalContext.current
    var mobileValue by remember { mutableStateOf("") }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Black)
            .padding(top = 33.dp, start = 25.dp, end = 25.dp)
    ) {
        CustomTextView(
            textData = context.getString(R.string.login_account_text),
            fontSize = 20,
            fontFamily = FontFamily(Font(R.font.lexend_semi_bold)),
            modifier = Modifier,
            textAlignment = TextAlign.Start
        )
        CustomTextView(
            textData = context.getString(R.string.welcome_text),
            fontSize = 14,
            textColor = colorResource(id = R.color.light_gray),
            fontFamily = FontFamily(Font(R.font.lexend_light)),
            modifier = Modifier.padding(top = 9.dp),
            textAlignment = TextAlign.Start
        )
        CustomTextView(
            textData = context.getString(R.string.phone_number_text),
            fontSize = 14,
            fontFamily = FontFamily(Font(R.font.lexend_semi_bold)),
            modifier = Modifier.padding(top = 50.dp),
            textAlignment = TextAlign.Start
        )
        CustomTextField(
            textFieldValue = mobileValue,
            modifier = Modifier.padding(top = 10.dp),
            hintText = context.getString(R.string.enter_phone_number_text),
            labelText = context.getString(R.string.enter_phone_number_text),
            keyboardInputType = KeyboardType.Number,
            imeAction = ImeAction.Done,
            onValueChanged = { text -> mobileValue = text },
        )

        CustomButton(
            text = context.getString(R.string.get_the_code_text),
            modifier = Modifier
                .padding(top = 16.dp)
                .height(50.dp)
                .fillMaxWidth(),
            corner = RoundedCornerShape(34.dp),
            onClick = {
                navController.navigate(RoutePath.HomeMainScreen.name)
            }
        )
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 30.dp), contentAlignment = Alignment.Center
        ) {
            VerticalSpacer(1, colorResource(id = R.color.silver_chalice_color))
            CustomTextView(
                textData = context.getString(R.string.sign_with_text),
                fontSize = 12,
                textColor = colorResource(id = R.color.silver_chalice_color),
                fontFamily = FontFamily(Font(R.font.lexend_medium)),
                modifier = Modifier
                    .background(Color.Black)
                    .padding(horizontal = 10.dp, vertical = 5.dp),
                textAlignment = TextAlign.Start
            )
        }

        Box(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 41.dp)
                .height(50.dp).clip(RoundedCornerShape(36.dp))
                .border(width = 1.dp, color = colorResource(id = R.color.silver_chalice_color), shape = RoundedCornerShape(36.dp))
                .background(colorResource(id = R.color.container_color))
                .clickable(onClick = {
//                    navController.navigate(RoutePath.AccurateDiagnosisScreen.name)
                }), contentAlignment = Alignment.Center
        ) {
            Image(painter = painterResource(id = R.drawable.ic_google), contentDescription ="google_image")
        }
    }
}
package com.lr.music.logicbeats.ui.screen.your_library

import android.content.Context
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import coil.compose.rememberAsyncImagePainter
import com.lr.music.logicbeats.R
import com.lr.music.logicbeats.common.CustomTextView
import com.lr.music.logicbeats.common.TopTitleWithCenterText
import com.lr.music.logicbeats.model.home_screen.LikedSongModel
import com.lr.music.logicbeats.model.playlist_model.PlaylistModel
import com.lr.music.logicbeats.model.playlist_model.RecentPlayedModel
import com.lr.music.logicbeats.ui.screen.liked_screen.LikedScreenViewModel

@Composable
fun YourLibraryScreen(navController: NavController, yourLibraryViewModel: YourLibraryViewModel = viewModel()) {
    var context = LocalContext.current
    Column(
        modifier = Modifier
            .fillMaxSize()
    ) {
        TitleBarUI(navController = navController, context = context, yourLibraryViewModel = yourLibraryViewModel)
        PlayListListUI(yourLibraryViewModel)
        CustomTextView(
            textData = context.getString(R.string.recent_played_text),
            fontSize = 14,
            textColor = colorResource(
                id = R.color.white
            ),
            fontFamily = FontFamily(Font(R.font.lexend_medium)),
            modifier = Modifier.padding(start = 14.dp),
            textAlignment = TextAlign.Start
        )
        RecentPlayedUI(yourLibraryViewModel)
    }
}

@Composable
fun TitleBarUI(navController: NavController, context: Context, yourLibraryViewModel: YourLibraryViewModel) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 10.dp, end = 10.dp), verticalAlignment = Alignment.CenterVertically
    ) {
        TopTitleWithCenterText(
            centerTitle = context.getString(R.string.your_library_text),
            showMenuButton = true,
            userImage = yourLibraryViewModel.userImage
        ) {
            navController.navigateUp()
        }
    }
}

@Composable
fun PlayListListUI(yourLibraryViewModel: YourLibraryViewModel) {
    Row {
        yourLibraryViewModel.playListArray.forEach { playListItem ->
            PlaylistItem(playListItem = playListItem) {

            }
        }
    }
}

@Composable
fun PlaylistItem(playListItem: PlaylistModel, onClick: () -> Unit) {
    Row(
        modifier = Modifier
            .defaultMinSize()
            .padding(horizontal = 5.dp, vertical = 15.dp)
    ) {
        Box(
            modifier = Modifier
                .width((LocalConfiguration.current.screenWidthDp.dp - 20.dp) / 4)
                .height(122.dp)
                .clip(RoundedCornerShape(53.dp))
                .background(
                    color = colorResource(
                        id = R.color.playlist_bg_color
                    )
                )
                .clickable { onClick() },
        ) {
            Column(modifier = Modifier.fillMaxSize(), horizontalAlignment = Alignment.CenterHorizontally) {
                Box(
                    modifier = Modifier
                        .size(58.dp)
                        .clip(CircleShape)
                        .background(Color.White), contentAlignment = Alignment.Center
                ) {
                    Image(painter = painterResource(id = playListItem.image), contentDescription = "IconImage", modifier = Modifier.size(24.dp))
                }
                Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
                    CustomTextView(
                        textData = playListItem.title,
                        fontSize = 12,
                        textColor = colorResource(
                            id = R.color.white
                        ),
                        fontFamily = FontFamily(Font(R.font.lexend_medium)),
                        modifier = Modifier,
                        textAlignment = TextAlign.Start
                    )
                }
            }

        }
    }
}

@Composable
fun RecentPlayedUI(yourLibraryViewModel: YourLibraryViewModel) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .verticalScroll(rememberScrollState()).padding(horizontal = 14.dp, vertical = 5.dp)
    ) {
        yourLibraryViewModel.recentPlayedArray.forEach { playedItem ->
            RecentPlayedItem(playedItem)
        }
    }
}

@Composable
fun RecentPlayedItem(recentItem: RecentPlayedModel) {
    Column(modifier = Modifier) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 18.dp), horizontalArrangement = Arrangement.SpaceBetween, verticalAlignment = Alignment.CenterVertically
        ) {
            Row(
                modifier = Modifier, verticalAlignment = Alignment.CenterVertically
            ) {
                Image(
                    painter = rememberAsyncImagePainter(recentItem.songImage),
                    contentDescription = "Person Image",
                    modifier = Modifier
                        .size(50.dp)
                        .clip(RoundedCornerShape(10.dp)), contentScale = ContentScale.Crop
                )
                Column(modifier = Modifier.padding(start = 17.dp)) {
                    CustomTextView(
                        textData = recentItem.songName,
                        fontSize = 14,
                        textColor = colorResource(id = R.color.white),
                        fontFamily = FontFamily(Font(R.font.lexend_medium)),
                        modifier = Modifier,
                        textAlignment = TextAlign.Start
                    )
                    Row(modifier = Modifier.padding(top = 7.dp), verticalAlignment = Alignment.CenterVertically) {
                        CustomTextView(
                            textData = recentItem.songDuration,
                            fontSize = 10,
                            textColor = colorResource(id = R.color.gray_color),
                            fontFamily = FontFamily(Font(R.font.lexend_regular)),
                            modifier = Modifier,
                            textAlignment = TextAlign.Start,
                        )
                        Box(
                            modifier = Modifier
                                .padding(horizontal = 5.dp)
                                .size(3.dp)
                                .clip(CircleShape)
                                .background(colorResource(id = R.color.gray_color))
                        )
                        CustomTextView(
                            textData = recentItem.singerName,
                            fontSize = 10,
                            textColor = colorResource(id = R.color.gray_color),
                            fontFamily = FontFamily(Font(R.font.lexend_regular)),
                            modifier = Modifier,
                            textAlignment = TextAlign.Start,
                        )
                    }
                }
            }
        }
        Spacer(
            modifier = Modifier
                .padding(top = 18.dp)
                .height(1.dp)
                .fillMaxWidth()
                .background(colorResource(id = R.color.divider_color))
        )
    }
}

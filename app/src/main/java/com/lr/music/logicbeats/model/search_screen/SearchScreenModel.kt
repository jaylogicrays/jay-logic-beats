package com.lr.music.logicbeats.model.search_screen

data class SearchScreenModel(
    val index : Int,
    val songImage: String,
    val songDuration: String,
    val songName: String,
    var singerName: String
)

package com.lr.music.logicbeats.ui.screen.liked_screen

import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.lifecycle.ViewModel
import com.lr.music.logicbeats.model.home_screen.LikedSongModel

class LikedScreenViewModel : ViewModel() {
    var likedSongArray: SnapshotStateList<LikedSongModel> = addLikedSongArray()

}

fun addLikedSongArray(): SnapshotStateList<LikedSongModel> {
    return mutableStateListOf(
        LikedSongModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem Ispum is Simply",
            "Kumar Sanu, Alka Yagni", true
        ),
        LikedSongModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem Ispum is Simply",
            "Kumar Sanu, Alka Yagni", true
        ), LikedSongModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem Ispum is Simply",
            "Kumar Sanu, Alka Yagni", true
        ), LikedSongModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem Ispum is Simply",
            "Kumar Sanu, Alka Yagni", true

        ), LikedSongModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem Ispum is Simply",
            "Kumar Sanu, Alka Yagni", true
        ), LikedSongModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem Ispum is Simply",
            "Kumar Sanu, Alka Yagni", true
        ), LikedSongModel(
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "02:05",
            "Lorem Ispum is Simply",
            "Kumar Sanu, Alka Yagni", true
        )
    )
}
package com.lr.music.logicbeats.ui.screen.liked_screen

import android.content.Context
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import coil.compose.rememberAsyncImagePainter
import com.lr.music.logicbeats.R
import com.lr.music.logicbeats.common.CustomTextView
import com.lr.music.logicbeats.common.TopTitleWithCenterText
import com.lr.music.logicbeats.model.home_screen.LikedSongModel

@Composable
fun LikedSongScreen(navController: NavController, likedScreenViewModel: LikedScreenViewModel = viewModel()) {
    val context = LocalContext.current
    Column(
        modifier = Modifier
            .fillMaxSize().background(Color.Black)
    ) {
        TitleBarUI(navController = navController, context = context)
        LikedSongUI(likedScreenViewModel)
    }
}

@Composable
fun TitleBarUI(navController: NavController, context: Context) {
    Row(modifier = Modifier.fillMaxWidth()) {
        TopTitleWithCenterText(centerTitle = context.getString(R.string.liked_text)) {
            navController.navigateUp()
        }
    }
}

@Composable
fun LikedSongUI(likedScreenViewModel: LikedScreenViewModel) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .verticalScroll(rememberScrollState()).padding(horizontal = 14.dp, vertical = 20.dp)
    ) {
        likedScreenViewModel.likedSongArray.forEach { likedItem ->
            LikedSongItem(likedItem)
        }
    }
}

@Composable
fun LikedSongItem(likedDataItem: LikedSongModel) {
    Column(modifier = Modifier) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 18.dp), horizontalArrangement = Arrangement.SpaceBetween, verticalAlignment = Alignment.CenterVertically
        ) {
            Row(
                modifier = Modifier, verticalAlignment = Alignment.CenterVertically
            ) {
                Image(
                    painter = rememberAsyncImagePainter(likedDataItem.songImage),
                    contentDescription = "Person Image",
                    modifier = Modifier
                        .size(50.dp)
                        .clip(RoundedCornerShape(5.dp)), contentScale = ContentScale.Fit
                )
                Column(modifier = Modifier.padding(start = 17.dp)) {
                    CustomTextView(
                        textData = likedDataItem.songName,
                        fontSize = 14,
                        textColor = colorResource(id = R.color.white),
                        fontFamily = FontFamily(Font(R.font.lexend_medium)),
                        modifier = Modifier,
                        textAlignment = TextAlign.Start
                    )
                    Row(modifier = Modifier.padding(top = 7.dp), verticalAlignment = Alignment.CenterVertically) {
                        CustomTextView(
                            textData = likedDataItem.songDuration,
                            fontSize = 10,
                            textColor = colorResource(id = R.color.gray_color),
                            fontFamily = FontFamily(Font(R.font.lexend_regular)),
                            modifier = Modifier,
                            textAlignment = TextAlign.Start,
                        )
                        Box(
                            modifier = Modifier
                                .padding(horizontal = 5.dp)
                                .size(3.dp)
                                .clip(CircleShape)
                                .background(colorResource(id = R.color.gray_color))
                        )
                        CustomTextView(
                            textData = likedDataItem.singerName,
                            fontSize = 10,
                            textColor = colorResource(id = R.color.gray_color),
                            fontFamily = FontFamily(Font(R.font.lexend_regular)),
                            modifier = Modifier,
                            textAlignment = TextAlign.Start,
                        )
                    }
                }
            }
            Row(verticalAlignment = Alignment.CenterVertically) {
                Box(modifier = Modifier.size(32.dp), contentAlignment = Alignment.Center){
                    Image(
                        painter = painterResource(id = if (likedDataItem.isLiked) R.drawable.ic_like else R.drawable.ic_unlike),
                        contentDescription = "Like Image",
                        modifier = Modifier
                            .size(22.dp),
                        contentScale = ContentScale.Fit
                    )
                }

                Box(
                    modifier = Modifier
                        .padding(start = 14.dp)
                        .size(30.dp)
                        .clip(RoundedCornerShape(5.dp)),
                    contentAlignment = Alignment.Center
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.ic_menu),
                        contentDescription = "Menu Image",
                        modifier = Modifier
                            .width(12.dp),
                        contentScale = ContentScale.Fit
                    )
                }
             }
        }
        Spacer(
            modifier = Modifier
                .padding(top = 18.dp)
                .height(1.dp)
                .fillMaxWidth()
                .background(colorResource(id = R.color.divider_color))
        )
    }
}
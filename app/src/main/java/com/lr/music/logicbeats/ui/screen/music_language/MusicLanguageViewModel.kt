package com.lr.music.logicbeats.ui.screen.music_language

import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.lifecycle.ViewModel
import com.lr.music.logicbeats.R
import com.lr.music.logicbeats.model.music_language.MusicLanguageModel

class MusicLanguageViewModel : ViewModel() {
    val singerImage = "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg"
    var musicLanguageArray: SnapshotStateList<MusicLanguageModel> = addMusicLanguageArray()
    var selectedColorArray =
        arrayListOf(R.color.light_salmon_pink_color, R.color.denim_blue_color, R.color.purple_mimosa_color, R.color.pale_slate_color)
}

fun addMusicLanguageArray(): SnapshotStateList<MusicLanguageModel> {
    return mutableStateListOf(
        MusicLanguageModel(
            "हिंदी",
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Hindi",
            true
        ),
        MusicLanguageModel(
            "English",
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "English",
            true
        ),
        MusicLanguageModel(
            "ગુજરાતી",
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Gujarati",
            true
        ),
        MusicLanguageModel(
            "Punjabi",
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Punjabi",
            true
        ),
        MusicLanguageModel(
            "हरियाणवी",
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "haryanvi",
            false
        ),
        MusicLanguageModel(
            "राजस्थानी",
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Rajasthani",
            false
        ),
        MusicLanguageModel(
            "भोजपुरी",
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Bhojpuri",
            false
        ),
        MusicLanguageModel(
            "اردو",
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Urdu",
            false
        ),
        MusicLanguageModel(
            "తెలుగు",
            "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg",
            "Telgu",
            false
        ),
    )
}
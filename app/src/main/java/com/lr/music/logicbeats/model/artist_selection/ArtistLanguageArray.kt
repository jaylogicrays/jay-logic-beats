package com.lr.music.logicbeats.model.artist_selection

import org.intellij.lang.annotations.Language

data class ArtistLanguageArray(var index : Int,var artistImage: String, val artistName: String, var isArtistSelected: Boolean, var language: String)

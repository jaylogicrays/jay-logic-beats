package com.lr.music.logicbeats.model.home_screen

data class DiscoverTitleDataModel(
    val songImage: String,
    val songDuration: String,
    val songName: String,
    var singerName: String,
    var isLiked: Boolean,
    var isPlaying: Boolean
)
package com.lr.music.logicbeats.model.now_playing

data class NowPlayingModel(val songImage: String, val songUrl : String,val songTitle: String, val songSubTitle: String, val isFav: Boolean)

package com.lr.music.logicbeats.ui.screen.artist_selection

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.lifecycle.ViewModel
import com.lr.music.logicbeats.model.artist_selection.ArtistCategoryModel
import com.lr.music.logicbeats.model.artist_selection.ArtistLanguageArray
import com.lr.music.logicbeats.model.artist_selection.ArtistSelectionData

class ArtistSelectionViewModel : ViewModel() {
    var textField = mutableStateOf("")

    //    var artistDataArray: SnapshotStateList<ArtistSelectionData> = getArtistSelectionData()
    var artistDataArray: SnapshotStateList<ArtistLanguageArray> = getArtistData()
    var selectedSongArray : SnapshotStateList<ArtistLanguageArray> = mutableStateListOf()
    var artistLanguageArray : SnapshotStateList<ArtistCategoryModel> = mutableStateListOf(ArtistCategoryModel("Hindi",false),ArtistCategoryModel("International",false),ArtistCategoryModel("Punjabi",false))
    var artistSongSelectedIndex by mutableStateOf(-1)
}

fun getArtistData(): SnapshotStateList<ArtistLanguageArray> {
    return mutableStateListOf(
        ArtistLanguageArray(0, "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "Alka Yagni", false, "Hindi"),
        ArtistLanguageArray(1, "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "Arjit Singh", false, "Hindi"),
        ArtistLanguageArray(2, "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "Katy Perry", true, "International"),
        ArtistLanguageArray(3, "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "Maddana", true, "International"),
        ArtistLanguageArray(4, "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "Diljit Dosanjh", false, "Punjabi"),
        ArtistLanguageArray(5, "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "Guru Randhawa", false, "Punjabi"),
        ArtistLanguageArray(6, "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "Jassie Gill", false, "Punjabi"),
        ArtistLanguageArray(7, "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "Taylor swift", false, "International"),
        ArtistLanguageArray(8, "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "Katy Perry", false, "International"),
        ArtistLanguageArray(9, "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "Honey Singh", false, "Hindi"),
        ArtistLanguageArray(10, "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "Sonu Nigam", false, "Hindi"),
        ArtistLanguageArray(11, "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "Alka Yagni", false, "Hindi"),
        ArtistLanguageArray(12, "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "Gurdas Maan", false, "Punjabi"),
        ArtistLanguageArray(13, "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "Harrdy Sandhu", false, "Punjabi"),
        ArtistLanguageArray(14, "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "Diljit Dosanjh", false, "Punjabi"),
        ArtistLanguageArray(15, "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "Maddana", false, "International"),
        ArtistLanguageArray(16, "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "Taylor swift", false, "International"),
        ArtistLanguageArray(17, "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "Arjit Singh", false, "Hindi"),
        ArtistLanguageArray(18, "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "Honey Singh", false, "Hindi"),
        ArtistLanguageArray(19, "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "Sonu Nigam", false, "Hindi"),
        ArtistLanguageArray(20, "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "Guru Randhawa", false, "Punjabi"),
        ArtistLanguageArray(21, "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "Jassie Gill", false, "Punjabi"),
        ArtistLanguageArray(22, "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "Gurdas Maan", false, "Punjabi"),
        ArtistLanguageArray(23, "https://semicolonspace.com/wp-content/uploads/2023/02/forest.jpg", "Harrdy Sandhu", false, "Punjabi"),
    )
}

fun getArtistSelectionData(): SnapshotStateList<ArtistSelectionData> {
    return mutableStateListOf(
        ArtistSelectionData(
            "Hindi",
            arrayListOf(
            ), false
        ),
        ArtistSelectionData(
            "International",
            arrayListOf(
            ), false
        ),
        ArtistSelectionData(
            "Punjabi",
            arrayListOf(
            ), false
        ),
    )
}
package com.lr.music.logicbeats.ui.screen.create_new_playlist

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.lr.music.logicbeats.R
import com.lr.music.logicbeats.common.CustomButton
import com.lr.music.logicbeats.common.CustomTextField
import com.lr.music.logicbeats.common.CustomTextView
import com.lr.music.logicbeats.common.VerticalSpacer

@Composable
fun CreateNewPlaylistScreen(navController: NavHostController, createNewPlaylistViewModel: CreateNewPlaylistViewModel = viewModel()) {
    val context = LocalContext.current
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(colorResource(id = R.color.black))
            .padding(horizontal = 14.dp), verticalArrangement = Arrangement.Center, horizontalAlignment = Alignment.CenterHorizontally
    ) {
        CustomTextView(
            textData = context.getString(R.string.playlist_name),
            fontSize = 18,
            textColor = colorResource(id = R.color.white),
            fontFamily = FontFamily(Font(R.font.lexend_regular)),
            modifier = Modifier.padding(top = 10.dp),
            textAlignment = TextAlign.Start
        )

        CustomEditTextField(createNewPlaylistViewModel)
        VerticalSpacer(1, colorResource(id = R.color.divider_color))
        Row(modifier = Modifier.fillMaxWidth().padding(top = 35.dp)) {
            CustomButton(
                text = LocalContext.current.getString(R.string.cancel_text),
                modifier = Modifier
                    .height(50.dp).padding(end = 5.dp)
                    .fillMaxWidth().weight(0.5f),
                fontColor = Color.White,
                fontSize = 16,
                fontFamily = FontFamily(Font(R.font.lexend_semi_bold)),
                backgroundColor = ButtonDefaults.buttonColors(Color.Transparent),
                corner = RoundedCornerShape(34.dp),
                borderStroke = BorderStroke(
                    width = 1.dp,
                    color = colorResource(id = R.color.divider_color)
                ),
                onClick = {
                }
            )
            CustomButton(
                text = LocalContext.current.getString(R.string.create_text),
                modifier = Modifier
                    .height(50.dp).padding(start = 5.dp)
                    .fillMaxWidth().weight(0.5f),
                fontColor = Color.Black,
                fontSize = 16,
                fontFamily = FontFamily(Font(R.font.lexend_semi_bold)),
                backgroundColor = ButtonDefaults.buttonColors(colorResource(id = R.color.white)),
                corner = RoundedCornerShape(34.dp),
                borderStroke = BorderStroke(
                    width = 1.dp,
                    color = colorResource(id = R.color.divider_color)
                ),
                onClick = {
                }
            )
        }
    }
}

@Composable
fun CustomEditTextField(createNewPlaylistViewModel: CreateNewPlaylistViewModel) {
    OutlinedTextField(
        value = createNewPlaylistViewModel.textField,
        onValueChange = { createNewPlaylistViewModel.textField = it },
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 122.dp),
        textStyle = TextStyle(
            fontFamily = FontFamily(Font(R.font.lexend_light)),
            fontWeight = FontWeight(400),
            fontSize = 14.sp,
            color = Color.White, textAlign = TextAlign.Center
        ),
        placeholder = {
            Text(
                modifier = Modifier.fillMaxWidth(),
                text = "Enter Playlist name",
                style = TextStyle(
                    fontFamily = FontFamily(Font(R.font.lexend_light)),
                    fontWeight = FontWeight(400),
                    fontSize = 14.sp,
                    color = Color.White, textAlign = TextAlign.Center
                )
            )
        },
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Text),
        singleLine = true,
        maxLines = 1,
        minLines = 1,
        shape = RoundedCornerShape(0.dp),
        colors = OutlinedTextFieldDefaults.colors(
            focusedTextColor = colorResource(id = R.color.container_color),
            cursorColor = colorResource(id = R.color.container_color),
            focusedBorderColor = Color.Transparent,
            unfocusedBorderColor = Color.Transparent,
            unfocusedContainerColor = Color.Transparent,
            focusedContainerColor = Color.Transparent,
        )
    )
}
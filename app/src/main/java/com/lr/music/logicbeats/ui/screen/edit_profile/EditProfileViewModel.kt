package com.lr.music.logicbeats.ui.screen.edit_profile

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel

class EditProfileViewModel : ViewModel() {
    var nameTextField by mutableStateOf("")
    var emailTextField by mutableStateOf("")
    var mobileNumberTextField by mutableStateOf("")
    var ageTextField by mutableStateOf("")
    var genderTextField by mutableStateOf("")
}